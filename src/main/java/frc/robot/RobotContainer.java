// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import frc.robot.Constants.IntakeShooterSubsystemConstants;
import frc.robot.Constants.LimeLightConstants;
import frc.robot.Constants.OperatorConstants;
import frc.robot.subsystems.ClimbSubsystem;
import frc.robot.subsystems.IntakeShooterSubsystem;
import frc.robot.subsystems.LEDSubsystem;
import frc.robot.subsystems.LimelightHelpers;
import frc.robot.subsystems.RainbowSubsytem;
import frc.robot.subsystems.testingsubsytem;
import frc.robot.subsystems.testingvelsubsystem;
import frc.robot.commands.swervedrive.drivebase.AbsoluteDrive;
import frc.robot.commands.swervedrive.drivebase.AbsoluteDriveAdv;
import frc.robot.commands.swervedrive.drivebase.AbsoluteFieldDrive;
import frc.robot.subsystems.swervedrive.SwerveSubsystem;
import frc.robot.util.RainbowMode;
import edu.wpi.first.wpilibj.Servo;
import java.io.File;
import com.ctre.phoenix.motorcontrol.IFollower;
import com.pathplanner.lib.auto.AutoBuilder;
import com.pathplanner.lib.auto.NamedCommands;
import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj.AddressableLED;
import edu.wpi.first.wpilibj.AddressableLEDBuffer;
import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj.RobotBase;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Commands;
import edu.wpi.first.wpilibj2.command.Commands.*;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.button.CommandJoystick;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import edu.wpi.first.wpilibj2.command.button.Trigger;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import frc.robot.subsystems.swervedrive.SwerveSubsystem;
import frc.robot.subsystems.BlinkSubSystem;
import java.io.File;
import frc.robot.commands.*;

/**
 * This class is where the bulk of the robot should be declared. Since
 * Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in
 * the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of
 * the robot (including
 * subsystems, commands, and trigger mappings) should be declared here.
 */

public class RobotContainer {
  // The robot's subsystems and commands are defined here...

  private final ClimbSubsystem m_Climb = new ClimbSubsystem();
  private final BlinkSubSystem m_Servo = new BlinkSubSystem();
  private final IntakeShooterSubsystem m_Shooter_Intake = new IntakeShooterSubsystem(m_Servo);
  private final RainbowMode m_rainbow = new RainbowMode(false);
  private final RainbowSubsytem m_RainbowSubsystem = new RainbowSubsytem(m_rainbow);
  private final LEDSubsystem m_LEDSubsystem;
  private final AddressableLED m_LED;
  private final AddressableLEDBuffer m_LEDBuffer;   
  private final SwerveSubsystem m_drivebase = new SwerveSubsystem(new File(Filesystem.getDeployDirectory(), "swerve"));

  // set up autos for smart dashboard
  private final SendableChooser<Command> m_autoChooser;

  XboxController m_driverXbox = new XboxController(OperatorConstants.kDriverControllerPort);
  CommandXboxController m_manipulator = new CommandXboxController(OperatorConstants.kManipulatorControllerPort);
  CommandXboxController m_pitController = new CommandXboxController(OperatorConstants.kPitControllerPort);
  private int m_DriveInvert;
  private boolean m_isRedAlliance;


  /**
   * The container for the robot. Contains subsystems, OI devices, and commands.
   */
  public RobotContainer(AddressableLED p_LED, AddressableLEDBuffer p_LEDBuffer) {

    m_LED = p_LED;
    m_LEDBuffer = p_LEDBuffer;
    
    m_LEDSubsystem = new LEDSubsystem(p_LED, p_LEDBuffer);
    // Configure the trigger bindings
    this.configureBindings();
    this.initializeDefaultCommands();
    LimelightHelpers.setLEDMode_ForceOff(LimeLightConstants.kLimelightName);
    DriverStation.silenceJoystickConnectionWarning(true);

        
    AbsoluteDriveAdv closedAbsoluteDriveAdv = new AbsoluteDriveAdv(m_drivebase,
        () -> MathUtil.applyDeadband(-(m_driverXbox.getLeftY() * m_DriveInvert),
            OperatorConstants.LEFT_Y_DEADBAND),
        () -> MathUtil.applyDeadband(-(m_driverXbox.getLeftX() * m_DriveInvert),
            OperatorConstants.LEFT_X_DEADBAND),
        () -> MathUtil.applyDeadband(-(m_driverXbox.getRightX()),
            OperatorConstants.RIGHT_X_DEADBAND),
        m_driverXbox::getYButtonPressed,
        m_driverXbox::getAButtonPressed,
        m_driverXbox::getXButtonPressed,
        m_driverXbox::getBButtonPressed);

    // Applies deadbands and inverts controls because joysticks
    // are back-right positive while robot
    // controls are front-left positive
    // left stick controls translation
    // right stick controls the desired angle NOT angular rotation
    Command driveFieldOrientedDirectAngle = m_drivebase.driveCommand(
        () -> MathUtil.applyDeadband(-(m_driverXbox.getLeftY() * m_DriveInvert), OperatorConstants.LEFT_Y_DEADBAND),
        () -> MathUtil.applyDeadband(-(m_driverXbox.getLeftX() * m_DriveInvert), OperatorConstants.LEFT_X_DEADBAND),
        () -> -(m_driverXbox.getRightX()),
        () -> -(m_driverXbox.getRightY()));

    // Applies deadbands and inverts controls because joysticks
    // are back-right positive while robot
    // controls are front-left positive
    // left stick controls translation
    // right stick controls the angular velocity of the robot
    Command driveFieldOrientedAnglularVelocity = m_drivebase.driveCommand(
        () -> MathUtil.applyDeadband(-(m_driverXbox.getLeftY() * m_DriveInvert), OperatorConstants.LEFT_Y_DEADBAND),
        () -> MathUtil.applyDeadband(-(m_driverXbox.getLeftX() * m_DriveInvert), OperatorConstants.LEFT_X_DEADBAND),
        () -> (m_driverXbox.getRightX()));

    Command driveFieldOrientedDirectAngleSim = m_drivebase.simDriveCommand(
        () -> MathUtil.applyDeadband(m_driverXbox.getLeftY(), OperatorConstants.LEFT_Y_DEADBAND),
        () -> MathUtil.applyDeadband(m_driverXbox.getLeftX(), OperatorConstants.LEFT_X_DEADBAND),
        () -> m_driverXbox.getRawAxis(2));

    m_drivebase.setDefaultCommand(
        !RobotBase.isSimulation() ? driveFieldOrientedAnglularVelocity : driveFieldOrientedDirectAngleSim);
    // !RobotBase.isSimulation() ? closedAbsoluteDriveAdv :
    // driveFieldOrientedDirectAngleSim);

    NamedCommands.registerCommand("shoot", new DumbShooter(m_Shooter_Intake));
    NamedCommands.registerCommand("intakeground", new FloorPickup(m_Shooter_Intake).withTimeout(2));
    NamedCommands.registerCommand("TrackandShoot", new TrackAndShootAuto(m_drivebase, m_Shooter_Intake, m_isRedAlliance));
    NamedCommands.registerCommand("intakeground1",new FloorPickup1(m_Shooter_Intake));
    NamedCommands.registerCommand("intakeground2",new FloorPickup2(m_Shooter_Intake));
    NamedCommands.registerCommand("Shootkindasmart", new ShootSpeaker(m_Shooter_Intake));
    // Build an auto chooser. This will use Commands.none() as the default option.
    m_autoChooser = AutoBuilder.buildAutoChooser();
    SmartDashboard.putData("Auto Chooser", m_autoChooser);

  } // End of RobotContainer constructor

  /**
   * Use this method to define your trigger->command mappings. Triggers can be
   * created via the
   * {@link Trigger#Trigger(java.util.function.BooleanSupplier)} constructor with
   * an arbitrary predicate, or via the
   * named factories in
   * {@link edu.wpi.first.wpilibj2.command.button.CommandGenericHID}'s subclasses
   * for
   * {@link CommandXboxController
   * Xbox}/{@link edu.wpi.first.wpilibj2.command.button.CommandPS4Controller PS4}
   * controllers or {@link edu.wpi.first.wpilibj2.command.button.CommandJoystick
   * Flight joysticks}.
   */

  /**
   * Use this method to define your trigger->command mappings. Triggers can be
   * created via the
   * {@link Trigger#Trigger(java.util.function.BooleanSupplier)} constructor with
   * an arbitrary
  
   * predicate, or via the named factories in {@link
   * edu.wpi.first.wpilibj2.command.button.CommandGenericHID}'s subclasses for
   * {@link
   * CommandXboxController
   * Xbox}/{@link edu.wpi.first.wpilibj2.command.button.CommandPS4Controller
   * PS4} controllers or
   * {@link edu.wpi.first.wpilibj2.command.button.CommandJoystick Flight
   * joysticks}.
   */
  private void configureBindings() {

    // *** DRIVER CONTROLLER BINDINGS ***
    
    // A: Zero Gyro
    new JoystickButton(m_driverXbox, 1).onTrue((new InstantCommand(m_drivebase::zeroGyro)));

    // B: Turn to Amp
    new JoystickButton(m_driverXbox, 2).whileTrue(new RotateToAmp(m_drivebase, m_isRedAlliance,
       () -> MathUtil.applyDeadband(-(m_driverXbox.getLeftX() * m_DriveInvert), OperatorConstants.LEFT_X_DEADBAND),
       () -> MathUtil.applyDeadband(-(m_driverXbox.getLeftY() * m_DriveInvert), OperatorConstants.LEFT_Y_DEADBAND)));

    // X: Intake Expeller
    new JoystickButton(m_driverXbox, 3).whileTrue(new IntakeExpeller(m_Shooter_Intake));

    // Y: Turn to Source
    new JoystickButton(m_driverXbox, 4).whileTrue(new RotateToSource(m_drivebase, m_isRedAlliance,
       () -> MathUtil.applyDeadband(-(m_driverXbox.getLeftX() * m_DriveInvert), OperatorConstants.LEFT_X_DEADBAND),
       () -> MathUtil.applyDeadband(-(m_driverXbox.getLeftY() * m_DriveInvert), OperatorConstants.LEFT_Y_DEADBAND)));

    // LB: Quarter Speed Drive
    new JoystickButton(m_driverXbox, 5).whileTrue(new Quarterspeed(m_drivebase,
       () -> MathUtil.applyDeadband(-(m_driverXbox.getLeftY() *  m_DriveInvert), OperatorConstants.LEFT_Y_DEADBAND),
       () -> MathUtil.applyDeadband(-(m_driverXbox.getLeftX() * m_DriveInvert), OperatorConstants.LEFT_X_DEADBAND),
       () -> (m_driverXbox.getRightX())));

    // RB: Ground Intake
    new JoystickButton(m_driverXbox, 6).whileTrue(new FloorPickup(m_Shooter_Intake));

   
    // *** MANIPULATOR CONTROLLER BINDINGS ***

    // TESTING!!!!!!
    //m_manipulator.leftTrigger().onTrue(m_Shooter_Intake.runOnce(() -> m_Shooter_Intake.setFlipperToDeployed()));
    //m_manipulator.back().onTrue(m_Shooter_Intake.runOnce(() -> m_Shooter_Intake.setFlipperToStow()));
    //m_manipulator.x().onTrue(new ShootSpeaker(m_Shooter_Intake)); 

    // A: Track Speaker & Shoot
    // Without rotation
    //m_manipulator.a().whileTrue(new ShootSpeaker(m_Shooter_Intake));
    // With rotation
    m_manipulator.a().whileTrue(new TrackAndShoot(m_drivebase, m_Shooter_Intake, m_isRedAlliance,
     () -> MathUtil.applyDeadband(-(m_driverXbox.getLeftX() * m_DriveInvert), OperatorConstants.LEFT_X_DEADBAND),
     () -> MathUtil.applyDeadband(-(m_driverXbox.getLeftY() * m_DriveInvert), OperatorConstants.LEFT_Y_DEADBAND),
     () -> m_manipulator.leftTrigger().getAsBoolean()));

    // B: Amp Shoot
    m_manipulator.b().onTrue(new AmpShooter(m_Shooter_Intake));

    // X: Empty
    
    // Y: Dumbshoot
    m_manipulator.y().onTrue(new DumbShooter(m_Shooter_Intake));

    // D-pad UP: Climber Up
    m_manipulator.povUp().onTrue(m_Climb.runOnce(() -> m_Climb.RaiseClimber()));
   
    // D-pad DOWN: Climber Down
    m_manipulator.povDown().onTrue(m_Climb.runOnce(() -> m_Climb.LowerClimber()));

    // D-pad Left: PreClimb
    m_manipulator.povLeft().onTrue(new PreClimb(m_Shooter_Intake));

    // D-Pad Right: TrapShoot
    m_manipulator.povRight().onTrue(new TrapShooterCommandGroup(m_Shooter_Intake));
   
    // LB: Pass Note to Amp Position
    m_manipulator.leftBumper().onTrue(new PassNote(m_Shooter_Intake));

    // RB: Source Intake
    // without rotation setting
    m_manipulator.rightBumper().onTrue(new SourceIntake(m_Shooter_Intake));
    // with rotation setting
    //m_manipulator.rightBumper().onTrue(new SourceIntakeWithAngle(m_Shooter_Intake, m_drivebase, m_isRedAlliance,

    // RT: Empty

    // Start: Stow Shooter
    m_manipulator.start().onTrue(new StowMode(m_Shooter_Intake));

    // Back: Empty


    // PIT CONTROLLER BUTTON BINDINGS

    m_pitController.a().whileTrue(new ClimberReelIn(m_Climb));
    m_pitController.x().onTrue(new FloorPickup2(m_Shooter_Intake));
    m_pitController.b().onTrue(new FloorPickup1(m_Shooter_Intake));
  } // End of Button Bindings

  public Command getAutonomousCommand() {
    // An example command will be run in autonomous
    return m_autoChooser.getSelected();
  }

  public void setMotorBrake(boolean brake) {
    m_drivebase.setMotorBrake(brake);
  }

  public void setStowedMode() {
    m_Shooter_Intake.setShooterToStowed();
    m_Shooter_Intake.setFlipperToStow();
  
  }

  public void checkAllianceColor() {

        //Get alliance from DriverStation
        var Alliance = DriverStation.getAlliance();

        //set driver controller inversions and set alliance color boolean
        if(Alliance.isPresent() && Alliance.get() == DriverStation.Alliance.Red) {
          m_DriveInvert = -1;
          m_isRedAlliance = true;
        }
        else {
          m_DriveInvert = 1;
          m_isRedAlliance = false;
        }
  }

  public void setDriveMode() {
    // m_drivebase.setDefaultCommand();
  }

  private void initializeDefaultCommands() {

    // m_LEDSubsystem.setDefaultCommand(new LEDDefaultCommand(m_LEDSubsystem,
    // m_RainbowSubsystem));

  }

}

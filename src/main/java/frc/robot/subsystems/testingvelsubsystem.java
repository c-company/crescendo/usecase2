// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkLowLevel.MotorType;
import com.revrobotics.SparkPIDController.AccelStrategy;
import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.SparkPIDController;
import com.revrobotics.CANSparkBase.ControlType;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.IntakeShooterSubsystemConstants;

public class testingvelsubsystem extends SubsystemBase {
  private CANSparkMax m_testMotor;  
  private RelativeEncoder m_test_relative_encoder;
  private SparkPIDController m_test_PID_controller;
  
  
  /** Creates a new testingsubsytem. */
  public testingvelsubsystem() {
   //  left shooter 16
   // intake break sensor 8 

  
    m_testMotor = new CANSparkMax(15, MotorType.kBrushless); //primary
    m_testMotor.restoreFactoryDefaults();
    m_testMotor.setInverted(true);  
    m_testMotor.setIdleMode(CANSparkMax.IdleMode.kCoast);
   // m_test_relative_encoder = m_testMotor.getEncoder();
   // m_test_PID_controller = m_testMotor.getPIDController();
    //Establish PID Values for Velocity
   /* m_test_PID_controller.setP(IntakeShooterSubsystemConstants.kShooterRightMotorP);
    m_test_PID_controller.setI(IntakeShooterSubsystemConstants.kShooterRightMotorI);
    m_test_PID_controller.setD(IntakeShooterSubsystemConstants.kShooterRightMotorD);
    m_test_PID_controller.setIZone(IntakeShooterSubsystemConstants.kShooterRightMotorz);
    m_test_PID_controller.setFF(IntakeShooterSubsystemConstants.kShooterRightMotorFF);
    m_test_PID_controller.setOutputRange(IntakeShooterSubsystemConstants.kShooterRightMotorMinOutput,IntakeShooterSubsystemConstants.kShooterRightMotorMaxOutput);
    */
   




  }

  

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    // SmartDashboard.putNumber("velocity", m_test_relative_encoder.getVelocity());

  }
  public void SetVelocity (double p_velocity) {
    m_test_PID_controller.setReference(p_velocity,ControlType.kVelocity);
    
  }

  public void SetSmartVelocity (double p_velocity) {
     m_test_PID_controller.setReference(p_velocity,ControlType.kSmartVelocity);
    
  }
  public double GetVelocity () {
      return m_test_relative_encoder.getVelocity();
  }

  public void SetSpeed (double p_speed){
      m_testMotor.set(p_speed);

  }
 
}

package frc.robot.subsystems;
import edu.wpi.first.wpilibj.AddressableLED;
import edu.wpi.first.wpilibj.AddressableLEDBuffer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;


public class LEDSubsystem extends SubsystemBase {
  private final AddressableLED m_LED;
  private final AddressableLEDBuffer m_LEDBuffer;
  private int m_rainbowFirstPixleHue;
  private boolean m_flashing = false;

  /** Creates a new LEDSubsystem. */
  public LEDSubsystem(AddressableLED p_LED, AddressableLEDBuffer p_AddressableLEDBuffer) {
    m_LED = p_LED;
    m_LEDBuffer =  p_AddressableLEDBuffer;
      
   // setTargetColor(Constants.LEDs.kRed, Constants.LEDs.kSatDefault);
   // startLEDs();
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler runS
   
    SmartDashboard.putBoolean("Idiot",LimelightHelpers.hasTarget());
  }

  public void startLEDs() {
    m_LED.start();
  }

  public void stopLEDs() {
    m_LED.stop();
  }
  public void setLEDsGreen(){
  for (var i = 0; i < Constants.LEDs.kLength; i++) {
    m_LEDBuffer.setHSV(i, Constants.LEDs.kGreen, Constants.LEDs.kSatDefault, 128);
  }
    m_LED.setData(m_LEDBuffer);
  }

  public void setLEDsRed(){
    for (var i = 0; i < Constants.LEDs.kLength; i++) {
      m_LEDBuffer.setHSV(i, Constants.LEDs.kGreen, Constants.LEDs.kSatDefault, 128);
    }
    m_LED.setData(m_LEDBuffer);
  }  
  

  public void blinkTheLEDsGreen(){
   int i = 0;

  while(m_flashing){
   if(i == 100){
     setLEDsGreen();
     i++;
   }
   else if(i == 200){
     stopLEDs();
     i++;
   }
   else if(i == 300){
     setLEDsGreen();
     i++;
   }
   else if(i == 400){
     stopLEDs();
     i++;
   }
   else if(i == 500){
     setLEDsGreen();
     i++;
    }
   else if(i == 600){
     stopLEDs();
     i++;
   }
   else if(i == 700){
     setLEDsGreen();
     i++;
   }
   else if(i == 800){
     stopLEDs();
     i++;
   }
   else if(i == 900){
     setLEDsGreen();
     i++;
   }
   else if(i == 1000){
     stopLEDs(); 
     i++;
   }
   else if(i > 1000){
    m_flashing = false;
    i = 0;
   }

   else {
     i++;
   }
   


  }
      
  }
 




  public void setRainbow()
  {
    for (var i = 0; i < m_LEDBuffer.getLength(); i++)
    {
       final var hue = (m_rainbowFirstPixleHue + (i * 180 / m_LEDBuffer.getLength())) % 180;
       m_LEDBuffer.setHSV(i, hue, 255, 128);
    }  
   m_rainbowFirstPixleHue += 3;

   m_rainbowFirstPixleHue %= 180;  
   
  }
  


  public void setTargetColor(int hue, int sat) {
    for (var i = 0; i < Constants.LEDs.kLength; i++) {
      m_LEDBuffer.setHSV(i, hue, sat, 128);
    }
    m_LED.setData(m_LEDBuffer);
  }

  
    
  
}


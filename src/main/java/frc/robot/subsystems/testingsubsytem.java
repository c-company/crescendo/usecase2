// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkLowLevel.MotorType;
import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.SparkPIDController;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.IntakeShooterSubsystemConstants;

public class testingsubsytem extends SubsystemBase {
  private CANSparkMax m_testMotor;
  private CANSparkMax m_testMotor2;
  private DigitalInput m_test_sensor;
  private RelativeEncoder m_test_relative_encoder;
  private SparkPIDController m_test_PID_controller;
  
  
  /** Creates a new testingsubsytem. */
  public testingsubsytem() {
 

 
    m_testMotor = new CANSparkMax(3, MotorType.kBrushless); //primary
    m_testMotor2 = new CANSparkMax(7,MotorType.kBrushless); // secondary
    m_testMotor.restoreFactoryDefaults();
    m_testMotor2.restoreFactoryDefaults();
    m_testMotor.setInverted(true);    // changed to inverted becasue of hardware configuration
    m_testMotor2.follow(m_testMotor,true);
    m_test_relative_encoder = m_testMotor.getEncoder();
    
    //m_test_sensor = new DigitalInput(7);

  }

  

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
     SmartDashboard.putNumber("encoder", m_test_relative_encoder.getPosition());

  }
  public void SetPercentage (double percentage) {
    m_testMotor.set(percentage);
    //m_testMotor2.set(percentage);//
  }
 
}

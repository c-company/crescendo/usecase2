// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkLowLevel.MotorType;
import com.revrobotics.SparkPIDController.AccelStrategy;

import frc.robot.Constants.LimeLightConstants;
import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.SparkPIDController;
import com.revrobotics.CANSparkBase.SoftLimitDirection;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.Constants.IntakeShooterSubsystemConstants;
import edu.wpi.first.wpilibj.Timer;
import java.lang.Math;


public class IntakeShooterSubsystem extends SubsystemBase {
  
  private CANSparkMax m_LeftShooterMotor;
  //private double m_ty = 0;
  //private double m_angleToSpeakerDegees = 0;
  private RelativeEncoder m_ShooterLeftMotorRelativeEncoder;
  private SparkPIDController m_ShooterLeftMotorPID;
  private CANSparkMax m_RightShooterMotor;
  private RelativeEncoder m_ShooterRightMotorRelativeEncoder;
  private SparkPIDController m_ShooterRightMotorPID;
  private CANSparkMax m_TopFloorIntakeMotor;
  private RelativeEncoder m_TopFloorIntakeMotorRelativeEncoder;
  private SparkPIDController m_TopFloorIntakeMotorPID;
  private CANSparkMax m_BottomFloorIntakeMotor;
  private RelativeEncoder m_BottomFloorIntakeMotorRelativeEncoder;
  private SparkPIDController m_BottomFloorIntakeMotorPID;
  private CANSparkMax m_IntakeConveyerMotor;
  private CANSparkMax m_HopupMotor;
  private DigitalInput m_SensorToStopFromSourceIntake;
  private DigitalInput m_SensorToStopFromGroundIntake;
  private CANSparkMax m_ShooterAngle;
  private RelativeEncoder m_ShooterAngleRelativeEncoder;
  private SparkPIDController m_ShooterAnglePID;
  private CANSparkMax m_ShooterCatcher;
  private CANSparkMax m_FlipperMotor;
  private RelativeEncoder m_FlipperRelativeEncoder;
  private SparkPIDController m_FlipperPID;
  private boolean m_RpmChecker;
  private BlinkSubSystem m_Servo;



  /** Creates a new ConveyorIntakeShooterSubsystem. */
  public IntakeShooterSubsystem(BlinkSubSystem p_Servo) {
    // initialize the shooter sub assembly 
    // includes the left and right shooter motors, the catcher, and hopup, and two led break sensors
    m_Servo = p_Servo;
    m_LeftShooterMotor = new CANSparkMax(IntakeShooterSubsystemConstants.kCANIDLeftShooterMotor, MotorType.kBrushless);
    m_RightShooterMotor = new CANSparkMax(IntakeShooterSubsystemConstants.kCANIDRightShooterMotor,MotorType.kBrushless);
    m_ShooterCatcher = new CANSparkMax(IntakeShooterSubsystemConstants.kCANIDShooterCatcherMotor, MotorType.kBrushless);
    m_HopupMotor = new CANSparkMax(IntakeShooterSubsystemConstants.kCANIDHopupShooterMotor, MotorType.kBrushless);
    // this sensor is on the ground intake side
    // returns true when the LED is not broken
    m_SensorToStopFromSourceIntake = new DigitalInput(IntakeShooterSubsystemConstants.kSensorFloorIntakeSide);
    // this sensor is on the source intake side
    // returns true when the LED is not broken
    m_SensorToStopFromGroundIntake = new DigitalInput(IntakeShooterSubsystemConstants.kSensorShooterSide);
    m_ShooterAngle = new CANSparkMax(IntakeShooterSubsystemConstants.kCANIDShooterAngleMotor, MotorType.kBrushless);
   
   //reset factory defaults for shooter sub assembly
    m_LeftShooterMotor.restoreFactoryDefaults();
    m_RightShooterMotor.restoreFactoryDefaults();
    m_ShooterCatcher.restoreFactoryDefaults();
    m_HopupMotor.restoreFactoryDefaults();
    m_ShooterAngle.restoreFactoryDefaults();
    
    // set inversions for shooter sub assembly
    m_LeftShooterMotor.setInverted(IntakeShooterSubsystemConstants.kLeftShooterInvert);
    m_RightShooterMotor.setInverted(IntakeShooterSubsystemConstants.kRightShooterInvert);
   
    m_ShooterCatcher.setInverted(IntakeShooterSubsystemConstants.kShooterCatcherMotorInvert);
    m_HopupMotor.setInverted(IntakeShooterSubsystemConstants.kHopupInvert);
    m_ShooterAngle.setInverted(IntakeShooterSubsystemConstants.kShooterAngleMotorInvert);

    // set motor modes  #TODO - set all the other ones that are set up right now only on the speed controllers
    m_RightShooterMotor.setIdleMode(CANSparkMax.IdleMode.kCoast);
    m_LeftShooterMotor.setIdleMode(CANSparkMax.IdleMode.kCoast);


    // initialize ground intake assembly
    m_IntakeConveyerMotor = new CANSparkMax(IntakeShooterSubsystemConstants.kCANIDIntakeConveyer,MotorType.kBrushless);
    m_TopFloorIntakeMotor = new CANSparkMax(IntakeShooterSubsystemConstants.kCANIDTopFloorIntakeMotor,MotorType.kBrushless);
    m_BottomFloorIntakeMotor = new CANSparkMax(IntakeShooterSubsystemConstants.kCANIDBottomFloorIntakeMotor,MotorType.kBrushless);
    m_FlipperMotor = new CANSparkMax(IntakeShooterSubsystemConstants.kCANIDFlipperMotor,MotorType.kBrushless);
    
    //restore to factory defaults for ground intake assembly
    m_IntakeConveyerMotor.restoreFactoryDefaults();
    m_TopFloorIntakeMotor.restoreFactoryDefaults();
    m_BottomFloorIntakeMotor.restoreFactoryDefaults();
    m_FlipperMotor.restoreFactoryDefaults();
    
    //set inversion for ground intake assembly
    m_IntakeConveyerMotor.setInverted(IntakeShooterSubsystemConstants.kIntakeConveyerInvert);    
    m_TopFloorIntakeMotor.setInverted(IntakeShooterSubsystemConstants.kTopFloorIntakeMotorInvert);
    m_BottomFloorIntakeMotor.setInverted(IntakeShooterSubsystemConstants.kBottomFloorIntakeMotorInvert);
    m_FlipperMotor.setInverted(IntakeShooterSubsystemConstants.kFlipperMotorInvert);
   

    // set up PID for left and right shooter motors
    //initalize left shooter motor encoder and PID controller
    m_ShooterLeftMotorRelativeEncoder = m_LeftShooterMotor.getEncoder();
    m_ShooterLeftMotorRelativeEncoder.setAverageDepth(4);
    m_ShooterLeftMotorRelativeEncoder.setMeasurementPeriod(8);
    // m_ShooterLeftMotorPID = m_LeftShooterMotor.getPIDController();
    // //Establish PID Values for velocity
    // m_ShooterLeftMotorPID.setP(IntakeShooterSubsystemConstants.kShooterLeftMotorP);
    // m_ShooterLeftMotorPID.setI(IntakeShooterSubsystemConstants.kShooterLeftMotorI);
    // m_ShooterLeftMotorPID.setD(IntakeShooterSubsystemConstants.kShooterLeftMotorD);
    // m_ShooterLeftMotorPID.setIZone(IntakeShooterSubsystemConstants.kShooterLeftMotorz);
    // m_ShooterLeftMotorPID.setFF(IntakeShooterSubsystemConstants.kShooterLeftMotorFF);
    // m_ShooterLeftMotorPID.setOutputRange(IntakeShooterSubsystemConstants.kShooterLeftMotorMinOutput,   IntakeShooterSubsystemConstants.kShooterLeftMotorMaxOutput);    
    // //Initialize left Shootor Motor Encoder and PID Controller 
    m_ShooterRightMotorRelativeEncoder = m_RightShooterMotor.getEncoder();
    m_ShooterRightMotorRelativeEncoder.setAverageDepth(4);
    m_ShooterRightMotorRelativeEncoder.setMeasurementPeriod(8);
    // m_ShooterRightMotorPID = m_RightShooterMotor.getPIDController();
    // //Establish PID Values for Velocity
    // m_ShooterRightMotorPID.setP(IntakeShooterSubsystemConstants.kShooterRightMotorP);
    // m_ShooterRightMotorPID.setI(IntakeShooterSubsystemConstants.kShooterRightMotorI);
    // m_ShooterRightMotorPID.setD(IntakeShooterSubsystemConstants.kShooterRightMotorD);
    // m_ShooterRightMotorPID.setIZone(IntakeShooterSubsystemConstants.kShooterRightMotorz);
    // m_ShooterRightMotorPID.setFF(IntakeShooterSubsystemConstants.kShooterRightMotorFF);
    // m_ShooterRightMotorPID.setOutputRange(IntakeShooterSubsystemConstants.kShooterRightMotorMinOutput,IntakeShooterSubsystemConstants.kShooterRightMotorMaxOutput);
    
    // ***note no PID for Hop Up or Catcher Motors speed control only

    //initalize angle motor encoder and PID controller
    m_ShooterAngleRelativeEncoder = m_ShooterAngle.getEncoder();
    m_ShooterAnglePID = m_ShooterAngle.getPIDController();
    // set ShooterAngle POSITIONAL PID Values
    m_ShooterAnglePID.setP(IntakeShooterSubsystemConstants.kShooterAngleP);
    m_ShooterAnglePID.setI(IntakeShooterSubsystemConstants.kShooterAngleI);
    m_ShooterAnglePID.setD(IntakeShooterSubsystemConstants.kShooterAngleD);
    m_ShooterAnglePID.setIZone(IntakeShooterSubsystemConstants.kShooterAnglez);
    m_ShooterAnglePID.setFF(IntakeShooterSubsystemConstants.kShooterAngleFF);
    m_ShooterAnglePID.setOutputRange(IntakeShooterSubsystemConstants.kShooterAnglerMinOutput,IntakeShooterSubsystemConstants.kShooterAnglerMaxOutput);
    int smartMotionSlot = 0;
    m_ShooterAnglePID.setSmartMotionMaxVelocity(IntakeShooterSubsystemConstants.kSmartMotionMaxVelocity, smartMotionSlot); // 5500 rpm
    m_ShooterAnglePID.setSmartMotionMinOutputVelocity(IntakeShooterSubsystemConstants.kSmartMotionMinOutputVelocity, smartMotionSlot);
    m_ShooterAnglePID.setSmartMotionMaxAccel(IntakeShooterSubsystemConstants.kSmartMotionMaxAccel, smartMotionSlot); // 4500 rpm 

     //setup encoders and PIDs for ground intake sub-assembly
    //initalize flipper  motor encoder and positional PID controller
    m_FlipperRelativeEncoder = m_FlipperMotor.getEncoder();
    m_FlipperPID = m_FlipperMotor.getPIDController();
    // set Flipper positoinal PID Values
    m_FlipperPID.setP(IntakeShooterSubsystemConstants.kFlipperP);
    m_FlipperPID.setI(IntakeShooterSubsystemConstants.kFlipperI);
    m_FlipperPID.setD(IntakeShooterSubsystemConstants.kFlipperD);
    m_FlipperPID.setIZone(IntakeShooterSubsystemConstants.kFlipperz);
    m_FlipperPID.setFF(IntakeShooterSubsystemConstants.kFlipperFF);
    m_FlipperPID.setOutputRange(IntakeShooterSubsystemConstants.kFlipperMinOutput,IntakeShooterSubsystemConstants.kFlipperMaxOutput);

    // m_BottomFloorIntakeMotorRelativeEncoder = m_BottomFloorIntakeMotor.getEncoder();
    // m_BottomFloorIntakeMotorPID = m_BottomFloorIntakeMotor.getPIDController();
    // // set ShooterAngle POSITIONAL PID Values
    // m_BottomFloorIntakeMotorPID.setP(IntakeShooterSubsystemConstants.kGrountIntakeBottomP);
    // m_BottomFloorIntakeMotorPID.setI(IntakeShooterSubsystemConstants.kGrountIntakeBottomI);
    // m_BottomFloorIntakeMotorPID.setD(IntakeShooterSubsystemConstants.kGrountIntakeBottomD);
    // m_BottomFloorIntakeMotorPID.setIZone(IntakeShooterSubsystemConstants.kGrountIntakeBottomz);
    // m_BottomFloorIntakeMotorPID.setFF(IntakeShooterSubsystemConstants.kGrountIntakeBottomFF);
    // m_BottomFloorIntakeMotorPID.setOutputRange(IntakeShooterSubsystemConstants.kGrountIntakeBottomMinOutput,IntakeShooterSubsystemConstants.kGrountIntakeBottomMaxOutput);
    
    // m_TopFloorIntakeMotorRelativeEncoder = m_TopFloorIntakeMotor.getEncoder();
    // m_TopFloorIntakeMotorPID = m_TopFloorIntakeMotor.getPIDController();
    // // set ShooterAngle POSITIONAL PID Values
    // m_TopFloorIntakeMotorPID.setP(IntakeShooterSubsystemConstants.kGrountIntakeTopP);
    // m_TopFloorIntakeMotorPID.setI(IntakeShooterSubsystemConstants.kGrountIntakeTopI);
    // m_TopFloorIntakeMotorPID.setD(IntakeShooterSubsystemConstants.kGrountIntakeTopD);
    // m_TopFloorIntakeMotorPID.setIZone(IntakeShooterSubsystemConstants.kGrountIntakeTopz);
    // m_TopFloorIntakeMotorPID.setFF(IntakeShooterSubsystemConstants.kGrountIntakeTopFF);
    // m_TopFloorIntakeMotorPID.setOutputRange(IntakeShooterSubsystemConstants.kGrountIntakeTopMinOutput,IntakeShooterSubsystemConstants.kGrountIntakeTopMaxOutput);
    // // SUPER IMPORTANT TO LEAVE THIS SOFT LIMIT IN, FAILURE COULD MEAN DEATH TO ROBOT
    // m_ShooterAngle.enableSoftLimit(SoftLimitDirection.kForward,true);
    // m_ShooterAngle.setSoftLimit(SoftLimitDirection.kForward, IntakeShooterSubsystemConstants.kShootrerAngleSoftLimit); 
    // // * there is no PID for conveyer motor speed control only

    //#TODO ADD SOFT LIMIT FOR FLIPPER

    
  
    //this.SetflipperSetPoint(false,IntakeShooterSubsystemConstants.kShooterRotationsStowed);
  }
  
  //**** sensor functions
  // returns true if it has a note returns false if it doesn't have a note
  public boolean getIntakeFromGroundSensorBlocked() {
    return !m_SensorToStopFromGroundIntake.get();
  }
  // returns true if it has a note returns false if it doesn't have a note
  public boolean getIntakeFromSourceSensorBlocked() {
    return !m_SensorToStopFromSourceIntake.get();
  }

  //***** limelight functions
  //get speakerAngle from Limelight camera
  public double getSpeakerAngle(){
    return (LimeLightConstants.kVerticleOffsetLimelightmountDegrees) + LimelightHelpers.getTY(LimeLightConstants.kLimelightName);
  }

  //*** functions for shooter angle sub-assembly
  // take a totoal number of rotations and return an angle

  // takes a degrees and returns a total number of rotations
  public double degreesToRotations(double degrees) {
    //make sure that we dont go over 360 pls
      double normalizeDegrees = degrees % 360.0;
      return normalizeDegrees / 360.0 * IntakeShooterSubsystemConstants.KShooterGearRatio; 
  }

  public double GetShooterToLimeLightHeightOffset(){
    return (LimeLightConstants.kLimeLightToHinge) - (LimeLightConstants.kHingeToShooter)*(Math.cos(Math.toRadians(getShooterSetPoint(true) + (IntakeShooterSubsystemConstants.kShooterWheelToHingeAngle))));
  }

  public double GetShooterToLimeLightWidthOffset(){
    return ((LimeLightConstants.kLimeLightToHinge) + (LimeLightConstants.kHingeToShooter)*(Math.sin(Math.toRadians(getShooterSetPoint(true) + (IntakeShooterSubsystemConstants.kShooterWheelToHingeAngle)))));
  }

  public double GetTanOfSpeakerAngle(){
    return (Math.tan(Math.toRadians(getSpeakerAngle())));
  }

  public double GetBaseSpeakerAngle(){
    return (((LimeLightConstants.kSpeakerBestHeightInchs - LimeLightConstants.kHeightOfLimelightInch) / (GetTanOfSpeakerAngle()))-(GetShooterToLimeLightWidthOffset()));
  }

  public double GetPerpendicularSpeaker(){
    return ((LimeLightConstants.kSpeakerBestHeightInchs) - (LimeLightConstants.kHeightOfLimelightInch + GetShooterToLimeLightHeightOffset()));
  }

  public double GetSpeakerAngleV2(){
    return (Math.toDegrees(Math.atan(GetPerpendicularSpeaker()/GetBaseSpeakerAngle())));
  }

  public double GetSpeakerDistance(){
    return ((LimeLightConstants.kSpeakerBestHeightInchs - LimeLightConstants.kHeightOfLimelightInch) / Math.tan(Math.toRadians(getSpeakerAngle())));
  }

  public double GetSpeakerHypotenuse(){
    return (LimeLightConstants.kSpeakerBestHeightInchs / Math.sin(Math.toRadians(GetSpeakerAngleV2())));
  }

  public double GetNoteToSpeakerTime(){
    return (GetSpeakerDistance() / (IntakeShooterSubsystemConstants.kNoteSpeedInches * Math.cos(Math.toRadians(GetSpeakerAngleV2()))));
  }

  public double GetNoteToSpeakerArc(){
    return (-9.8 * (Math.pow(GetNoteToSpeakerTime(),2)) / 2);
  }

  public double GetSpeakerHeightV2(){
    return (LimeLightConstants.kSpeakerBestHeightInchs - GetNoteToSpeakerArc());
  }
  
  public double GetSpeakerAngleV3(){
    return (Math.toDegrees(Math.atan((GetSpeakerHeightV2())/(GetSpeakerDistance()))));
  }

  public double GetNoteToSpeakerTimeV2(){
    return (GetSpeakerDistance() / (IntakeShooterSubsystemConstants.kNoteSpeedInches * Math.cos(Math.toRadians(GetSpeakerAngleV3()))));
  }

  public double GetNoteToSpeakerArcV2(){
    return (-9.8 * (Math.pow(GetNoteToSpeakerTimeV2(),2)) / 2);
  }

  public double GetSpeakerHeightV3(){
    return (LimeLightConstants.kSpeakerBestHeightInchs - GetNoteToSpeakerArcV2());
  }
  
  public double GetSpeakerAngleV4(){
    return (Math.atan((GetSpeakerHeightV3())/(GetSpeakerDistance())));
  }

  public double GetNoteToSpeakerTimeV3(){
    return (GetSpeakerDistance() / (IntakeShooterSubsystemConstants.kNoteSpeedInches * Math.cos(Math.toRadians(GetSpeakerAngleV4()))));
  }

  public double GetNoteToSpeakerArcV3(){
    return (-9.8 * (Math.pow(GetNoteToSpeakerTimeV3(),2)) / 2);
  }

  public double GetSpeakerHeightV4(){
    return (LimeLightConstants.kSpeakerBestHeightInchs - GetNoteToSpeakerArcV3());
  }
  
  public double GetSpeakerAngleV5(){
    return (Math.atan((GetSpeakerHeightV4())/(GetSpeakerDistance())));
  }

  //Old Code starts here
  // public double GetSpeakerAdjacentOld(){
  //   return (LimeLightConstants.kSpeakerBestHightInchs / Math.cos(GetSpeakerAngle()));
  // }

  // public double GetSpeakerHypotenuseOld(){
  //   return ((LimeLightConstants.kSpeakerBestHightInchs)/(Math.sin(GetSpeakerAngle())));
  // }


  // public double GetNoteToSpeakerTimeOld(){
  //   return (GetSpeakerHypotenuse());
  // }

  // public double GetNoteToSpeakerArkOld(){
  //   return ((Math.pow((-9.8*(GetNoteToSpeakerTime())),2))/2);
  // }

  // public double GetSpeekerHypotenuseV2(){
  //   return ((LimeLightConstants.kSpeakerBestHightInchs + GetNoteToSpeakerArk())/(Math.sin(GetSpeakerAngle())));
  // }

  // public double GetNoteToSpeakerTimeV2Old(){
  //   return (GetSpeekerHypotenuseV2());
  // }

  // public double GetNoteToSpeakerArkV2Old(){
  //   return ((Math.pow((-9.8*((0))),2))/2);
  // }

  // public double GetSpeekerOpposite(){
  //   return (LimeLightConstants.kSpeakerBestHightInchs + GetNoteToSpeakerArkV2());
  // }

  // public double GetSpeakerAngle2Old(){
  //   return (Math.atan((GetSpeekerOpposite())/(GetSpeakerAdjacent())));
  // }
  //Old code ends here
  public double GetLeftShooterMotorVelocity(){
    return m_ShooterLeftMotorRelativeEncoder.getVelocity();
  }

  public double GetRightShooterMotorVelocity(){
    return (m_ShooterRightMotorRelativeEncoder.getVelocity());
  }

  public double GetShooterAngleDegree(){
    return (m_ShooterAngleRelativeEncoder.getPosition() / IntakeShooterSubsystemConstants.KShooterGearRatio * 360.0);
  }

  // take a totoal number of rotations and return an angle
  public double rotationsToDegrees(double rotations){
   return rotations / IntakeShooterSubsystemConstants.KShooterGearRatio * 360.0;
  }
  
  
  /*set the shooter arm to a rotations or BS angle true use an angle, false use a rotations*/
  public void setShooterSetPoint(boolean p_type_true_if_angle, double p_value) {
    if (p_type_true_if_angle){
       double rotations = degreesToRotations(p_value);
       m_ShooterAnglePID.setReference(rotations, CANSparkMax.ControlType.kSmartMotion);
    }
    else if (!p_type_true_if_angle){
       m_ShooterAnglePID.setReference(p_value, CANSparkMax.ControlType.kSmartMotion );
    }   
  }
  //gets the shooter arm, true for BS angle, false for rotations
  public double getShooterSetPoint(boolean p_type_true_if_angle) {
    double value = 0;   
   if (p_type_true_if_angle)
       value = rotationsToDegrees(m_ShooterAngleRelativeEncoder.getPosition());
    else if (!p_type_true_if_angle)
       value = m_ShooterAngleRelativeEncoder.getPosition();   
   return value; 
  }  
  //**** shooter position set points
  //sets to the preknown # of rotations to stow the shooter arm
  public void setShooterToStowed() {
    setShooterSetPoint(false, IntakeShooterSubsystemConstants.kShooterRotationsStowed);
  }
  // sets to the preknown # of rotations to move the shooter arm to speaker shoot
  public void setShooterToManualSpeaker() {
    setShooterSetPoint(false,IntakeShooterSubsystemConstants.kShooterRotationsManualSpeaker);  
  }
  // sets to the preknown # of rotations to move the shooter arm to source pickup
  public void setShooterToSourcePickup() {
    setShooterSetPoint(false,IntakeShooterSubsystemConstants.kShooterRotationsSource);  
  }
  // set shooter to floor pickup position
  public void setShooterToFloorPickup(){
    setShooterSetPoint(false, IntakeShooterSubsystemConstants.kShooterRotationsGroundPickup);
  }

  // set shooter to pre-climb position
  public void setShooterToPreClimb(){
    setShooterSetPoint(false, IntakeShooterSubsystemConstants.kShooterRotationsPreClimb);
  }
  
  /*set the shooter arm to trap rotations*/
  public void setShooterToTrap() {
    setShooterSetPoint(false, IntakeShooterSubsystemConstants.kShooterRotationsTrap);  
  }
  // set the shooter arm to amp rotations
  public void setShooterToAmp() {
    setShooterSetPoint(false, IntakeShooterSubsystemConstants.kShooterRotationsAmp);  
  }
  /// **** motor control on shooter
  // set shooter wheels velocity with PID used in multiple ways
   public void setShooterWheelsVelocity (double p_LeftVelocity, double p_RightVelocity) {
      m_ShooterLeftMotorPID.setReference(p_LeftVelocity, CANSparkMax.ControlType.kVelocity);
      m_ShooterRightMotorPID.setReference(p_RightVelocity, CANSparkMax.ControlType.kVelocity);
  }
  // set shooter wheels velocity using percentage 
  public void setShooterWheelsSpeed (double p_LeftSpeed, double p_RightSpeed) {
      m_LeftShooterMotor.set(p_LeftSpeed);
      m_RightShooterMotor.set(p_RightSpeed);
  }

  public boolean checkShooterFullSpeed(){
      
    return(m_ShooterLeftMotorRelativeEncoder.getVelocity() >= IntakeShooterSubsystemConstants.kMotorFullSpeed);
    
  }

  public boolean checkShooterInPosition(double p_rotations){
    if (Math.abs(m_ShooterAngleRelativeEncoder.getPosition() - p_rotations) < IntakeShooterSubsystemConstants.kShooterAngleToleranceRotations)
      return true;
    else
      return false;
  } 

  // calculate necessary angle offset using distance from speaker
  public double calculateAngleOffset(double p_distance) {
  
      return (0.425 - (0.08 * p_distance) + (0.00125 * Math.pow(p_distance, 2)));
  }
  
  
  // Set Hopup Motor to a value
  public void setHopupPercentage(double percentage) {
    m_HopupMotor.set(percentage);
  }
  // Turn off Hop Up Motor
  public void disableHopupMotor() {
    m_HopupMotor.set(IntakeShooterSubsystemConstants.kMotorOffSpeed);
  }
  // sets speed of catcher motor this is setable by command cause we need it for trap shoot
  public void setCatcherPercentage(double p_speed) {
    m_ShooterCatcher.set(p_speed);  
  } 
  //Turn off Catcher Motor
  public void disableCatcherMotor(){
    m_ShooterCatcher.set(IntakeShooterSubsystemConstants.kMotorOffSpeed);
  }
  // *** Motor controllers on Flipper sub-assembly
  /** Enables the intake R & L flipper motors and conveyer motor */
  public void enableIntakeFlipperMotors() {
    m_TopFloorIntakeMotor.set(IntakeShooterSubsystemConstants.kIntakeTopFloorMotorSpeed);
    m_BottomFloorIntakeMotor.set(IntakeShooterSubsystemConstants.kIntakeBottomFloorMotorSpeed);
    setConveyerMotorSpeed(IntakeShooterSubsystemConstants.kGroundIntakeConveyorMotorSpeed);
  }
  /** Disables the intake top and bottom intake flipper  motors  and conveyer motor*/
  public void disableIntakeFlipperMotors() {
    m_TopFloorIntakeMotor.set(IntakeShooterSubsystemConstants.kMotorOffSpeed);
    m_BottomFloorIntakeMotor.set(IntakeShooterSubsystemConstants.kMotorOffSpeed);
    setConveyerMotorSpeed(IntakeShooterSubsystemConstants.kMotorOffSpeed);   
  }
  // set the speed of the conveyerMotor
  public void setConveyerMotorSpeed(double p_speed) {
    m_IntakeConveyerMotor.set(p_speed);   
  }

  //set the speed of the top intake flipper
  public void setTopFlipperMotorSpeed(double p_speed){
    m_TopFloorIntakeMotor.set(p_speed);
  }

  //set the speed of the bottom intake flipper
  public void setBottomFlipperMotorSpeed(double p_speed){
    m_BottomFloorIntakeMotor.set(p_speed);
  }

  /// **** Functions for Flipper Angle Management
   // takes a degrees and returns a total number of rotations
  public double flipperDegreesToRotations(double degrees) {
    //make sure that we dont go over 360 pls
      double normalizeDegrees = degrees % 360.0;
      return normalizeDegrees / 360.0 * IntakeShooterSubsystemConstants.KFlipperGearRatio; 
  }
  // changes rotations to degrees for flipper motor
  public double flipperRotationsToDegrees(double rotations){
   return rotations / IntakeShooterSubsystemConstants.KFlipperGearRatio * 360.0;
  }
  // Set the rotations or angle of the Intake Flippere
  public void setIntakeFlipperRotations(boolean p_type_true_if_angle, double p_value) {
    if (p_type_true_if_angle){
      double rotations = flipperDegreesToRotations(p_value);
      m_FlipperPID.setReference(rotations, CANSparkMax.ControlType.kPosition);
    }
    else {
      m_FlipperPID.setReference(p_value, CANSparkMax.ControlType.kPosition);
    }
    
  }
 //gets the flipper position, true for BS angle, false for rotations
  public double getFlipperSetPoint(boolean p_type_true_if_angle) {
    double value = 0;   
   if (p_type_true_if_angle)
       value = rotationsToDegrees(m_FlipperRelativeEncoder.getPosition());
    else if (!p_type_true_if_angle)
       value = m_FlipperRelativeEncoder.getPosition();   
   return value; 
  }
  /* set the flipper to stow position */
  
  public void setFlipperToStow() {
    setIntakeFlipperRotations(false, IntakeShooterSubsystemConstants.kFlipperStowPosition);  
  }
  public void setFlipperToDeployed() {
    setIntakeFlipperRotations(false, IntakeShooterSubsystemConstants.kFlipperDeployPosition);  
  }

  //Reel In Flipper - For Pit Controller
  public void ReelInFlipper() {
    m_FlipperMotor.set(IntakeShooterSubsystemConstants.kShooterReelInFlipperSpeed);
  }

  //Turn off Flipper motor
  public void FlipperOff() {
    m_FlipperMotor.set(IntakeShooterSubsystemConstants.kMotorOffSpeed);
  }
  
 
  
  

  
  @Override
  public void periodic() {
     SmartDashboard.putBoolean("getIntakeFromGroundSensorBlocked", getIntakeFromGroundSensorBlocked());
     SmartDashboard.putBoolean("getIntakeFromSourceSensorBlocked", getIntakeFromSourceSensorBlocked());
     SmartDashboard.putNumber("shooter rpm's",GetLeftShooterMotorVelocity());
    //  SmartDashboard.putNumber("Shooter rotations",getShooterSetPoint(false));
    //  SmartDashboard.putNumber("Shooter BS Angle",getShooterSetPoint(true));
     SmartDashboard.putNumber("Flipper rotations",getFlipperSetPoint(false));
     SmartDashboard.putNumber("Flipper BS Angle",getFlipperSetPoint(true));
    //  SmartDashboard.putNumber("lvelocity", m_ShooterLeftMotorRelativeEncoder.getVelocity());
    //  SmartDashboard.putNumber("rvelocity", m_ShooterRightMotorRelativeEncoder.getVelocity());

    var alliance = DriverStation.getAlliance();
    if(getIntakeFromGroundSensorBlocked() || getIntakeFromSourceSensorBlocked()) 
      m_Servo.Green();
    else if (alliance.isPresent() && (alliance.get() == DriverStation.Alliance.Red))
      m_Servo.Red();
    else if (alliance.isPresent() && (alliance.get() == DriverStation.Alliance.Blue))
      m_Servo.Blue();
    
  }
}

// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkLowLevel.MotorType;
import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.CANSparkBase.SoftLimitDirection;
import com.revrobotics.SparkPIDController;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.ClimbConstants;

public class ClimbSubsystem extends SubsystemBase {
  private RelativeEncoder m_ClimbRelativeEncoder;
  private CANSparkMax m_PrimeClimbMotor;
  private CANSparkMax m_FollowClimbMotor;
  private SparkPIDController m_ClimbPID;
  private boolean m_isClimberAtBottom;

  /** Creates a new ClimbSubsystem. */
  public ClimbSubsystem() {
    m_PrimeClimbMotor = new CANSparkMax(ClimbConstants.kCANIDLEADClimbMotor, MotorType.kBrushless);
    m_FollowClimbMotor = new CANSparkMax(ClimbConstants.kCANIDFollowClimbMotor, MotorType.kBrushless);
    m_PrimeClimbMotor.restoreFactoryDefaults();
    m_FollowClimbMotor.restoreFactoryDefaults();
    m_PrimeClimbMotor.setSmartCurrentLimit(ClimbConstants.kSmartCurrentLimit);
    m_FollowClimbMotor.setSmartCurrentLimit(ClimbConstants.kSmartCurrentLimit);
    // inverted based on testing on 2/23/2026 mac
    //IMPORTANT IF YOU CHANGE THESE SETTING YOU MAY CREATE A SHAFT PRETZEL AND RELEASE THE MAGIC SMOKE
    m_PrimeClimbMotor.setInverted(true);    
    m_FollowClimbMotor.follow(m_PrimeClimbMotor,true);
    m_ClimbRelativeEncoder = m_PrimeClimbMotor.getEncoder();
    m_ClimbPID = m_PrimeClimbMotor.getPIDController();
    m_PrimeClimbMotor.setSoftLimit(SoftLimitDirection.kForward,  ClimbConstants.kClimbSoftLimit);

    // set PID Values
    m_ClimbPID.setP(ClimbConstants.kClimbP);
    m_ClimbPID.setI(ClimbConstants.kClimbI);
    m_ClimbPID.setD(ClimbConstants.kClimbD);
    m_ClimbPID.setIZone(ClimbConstants.kClimbIz);
    m_ClimbPID.setFF(ClimbConstants.kClimbFF);
    m_ClimbPID.setOutputRange(ClimbConstants.kClimbMinOutput, ClimbConstants.kClimbMaxOutput);
    // int smartMotionSlot = 0;
    // m_ClimbPID.setSmartMotionMaxVelocity(ClimbConstants.kClimbSmartMaxVelocity, smartMotionSlot);
    // m_ClimbPID.setSmartMotionMinOutputVelocity(ClimbConstants.kClimbSmartMinOutputVelocity, smartMotionSlot);
    // m_ClimbPID.setSmartMotionMaxAccel(ClimbConstants.kClimbSmartMaxAccel, smartMotionSlot);

    
    // climber starts at bottom
    m_isClimberAtBottom = true; 
   }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    SmartDashboard.putNumber("Encoder Value For Climber", m_ClimbRelativeEncoder.getPosition());
    SmartDashboard.putBoolean("isClimberDown: ", m_isClimberAtBottom);
  }

  /**
   * Lowers the climber to the bottom position 
   */
  public void LowerClimber() {
    m_ClimbPID.setReference(ClimbConstants.kClimbBottomPosition, CANSparkMax.ControlType.kPosition);
    m_isClimberAtBottom = true;
  }
  

  /**
   * Raises the climber to the top position 
   */
  public void RaiseClimber() {
    m_ClimbPID.setReference(ClimbConstants.kClimbTopPosition, CANSparkMax.ControlType.kPosition);
    m_isClimberAtBottom = false;
  }

  // Reel in Climber - For Pit Controller
  public void ReelInClimber(){
    m_PrimeClimbMotor.set(ClimbConstants.kClimberReelInSpeed);
  }

  // Turn off Climber motor
  public void ClimberOff(){
    m_PrimeClimbMotor.set(ClimbConstants.kClimberOffSpeed);
  }

  /**
   * Status for the climber position down or not
   * @return boolean true if the climber is down
   */
  public boolean isClimberDown() {
    // is there a PID or encoder/motor controller check for the current position?
    return m_isClimberAtBottom;
  }

}

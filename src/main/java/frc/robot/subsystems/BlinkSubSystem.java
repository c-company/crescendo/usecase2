// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Servo;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class BlinkSubSystem extends SubsystemBase {
  /** Creates a new BlinkSubSystem. */
 
  private Servo m_blinkin_support_lights;
  private boolean m_blink_in_progress = false;
  
  public BlinkSubSystem() {
    m_blinkin_support_lights = new Servo(Constants.LEDs.support_lights);var alliance = DriverStation.getAlliance();
    if (alliance.get() == DriverStation.Alliance.Red)
      m_blinkin_support_lights.set(0.67);
    if (alliance.get() == DriverStation.Alliance.Blue)
      m_blinkin_support_lights.set(0.73);
    
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
 
    while (m_blink_in_progress) {
      //flash blinkin green twice then back to driver station default
      for(int i = 0; i < 100; i++){
        m_blinkin_support_lights.set(0.77 );// set to green
        if (i == 25)
            m_blinkin_support_lights.set(0.99);// set to black
        else if(i == 35)
            m_blinkin_support_lights.set(0.77 );// set to green
        else if (i == 60)
            m_blinkin_support_lights.set(0.99);// set to black
        else if (i == 70)
            m_blinkin_support_lights.set(0.77 );// set to set to green
        else if (i == 99){
            m_blinkin_support_lights.set(i);// set back to drive station default
            var alliance = DriverStation.getAlliance();
            if (alliance.get() == DriverStation.Alliance.Red)
                m_blinkin_support_lights.set(0.67);
            if (alliance.get() == DriverStation.Alliance.Blue)
                m_blinkin_support_lights.set(0.73);
            m_blink_in_progress = false;
        }
     }
    }

  }

  public void Blink() {
    m_blink_in_progress = true; 
  }

  public void Green() {
    m_blinkin_support_lights.set(.75);
  }

  public void Red() {
    m_blinkin_support_lights.set(.67);
  }

  public void Blue() {
    m_blinkin_support_lights.set(.73);
  }
}

// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj.AddressableLED;
import edu.wpi.first.wpilibj.AddressableLEDBuffer;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj.Timer;

/**
 * The VM is configured to automatically run this class, and to call the functions corresponding to
 * each mode, as described in the TimedRobot documentation. If you change the name of this class or
 * the package after creating this project, you must also update the build.gradle file in the
 * project.
 */
public class Robot extends TimedRobot {
  private Command m_autonomousCommand;
  private RobotContainer m_robotContainer;
  private static Robot m_instance;
  private AddressableLED m_LED;
  private AddressableLEDBuffer m_LEDBuffer;
  private int m_rainbowFirstPixelHue; 
  private int m_switchCycleCounter;
  private int m_switchCycleChange;
  private Timer m_disabledTimer;
  // store state of animation (off when robot is enabled)
  private boolean m_turn_off_rainbow = false;



  public Robot()
  {
    m_instance = this;    
  }

  public static Robot getInstance()
  {
    return m_instance;
  }
  /**
   * This function is run when the robot is first started up and should be used for any
   * initialization code.
   */
  @Override
  public void robotInit() {
    // Instantiate our RobotContainer.  This will perform all our button bindings, and put our
    // autonomous chooser on the dashboard.
     m_LED = new AddressableLED(Constants.LEDs.kPwmChannel);
    m_LEDBuffer = new AddressableLEDBuffer(Constants.LEDs.kLEDBufferLength);
    m_LED.setLength(m_LEDBuffer.getLength());
    m_rainbowFirstPixelHue = 0;// red*/
    m_switchCycleChange = 100; 
    m_switchCycleCounter = 0;
    m_robotContainer = new RobotContainer(m_LED, m_LEDBuffer);
    m_robotContainer.setStowedMode();

   // Create a timer to disable motor brake a few seconds after disable.  This will let the robot stop
    // immediately when disabled, but then also let it be pushed more 
    m_disabledTimer = new Timer();
  }

  /**
   * This function is called every 20 ms, no matter the mode. Use this for items like diagnostics
   * that you want ran during disabled, autonomous, teleoperated and test.
   *
   * <p>This runs after the mode specific periodic functions, but before LiveWindow and
   * SmartDashboard integrated updating.
   */
  @Override
  public void robotPeriodic() {
    // Runs the Scheduler.  This is responsible for polling buttons, adding newly-scheduled
    // commands, running already-scheduled commands, removing finished or interrupted commands,
    // and running subsystem periodic() methods.  This must be called from the robot's periodic
    // block in order for anything in the Command-based framework to work.
    CommandScheduler.getInstance().run();
    if(!m_turn_off_rainbow)
    {
        rainbow();
   
    }
  }

  /** This function is called once each time the robot enters Disabled mode. */
  @Override
  public void disabledInit() {
    m_robotContainer.setMotorBrake(true);
    m_disabledTimer.reset();
    m_disabledTimer.start();
  }

  @Override
  public void disabledPeriodic() {
    {
      m_robotContainer.setMotorBrake(false);
      m_disabledTimer.stop();
    
    }

  }

  /** This autonomous runs the autonomous command selected by your {@link RobotContainer} class. */
  @Override
  public void autonomousInit() {
    m_robotContainer.setMotorBrake(true);
    m_autonomousCommand = m_robotContainer.getAutonomousCommand();

    // schedule the autonomous command (example)
    if (m_autonomousCommand != null) {
      m_autonomousCommand.schedule();
    }
  }

  /** This function is called periodically during autonomous. */
  @Override
  public void autonomousPeriodic() {}

  @Override
  public void teleopInit() {
    // This makes sure that the autonomous stops running when
    // teleop starts running. If you want the autonomous to
    // continue until interrupted by another command, remove
    // this line or comment it out.
    if (m_autonomousCommand != null) {
      m_autonomousCommand.cancel();
    }

    m_robotContainer.setDriveMode();
    m_robotContainer.setMotorBrake(true);
    m_robotContainer.checkAllianceColor();
  }

  /** This function is called periodically during operator control. */
  @Override
  public void teleopPeriodic() {}
  
  public void rainbow() {
    // For every pixel
    for (var i = 100; i < Constants.LEDs.kLEDBufferLength; i++) {
      // Calculate the hue - hue is easier for rainbows because the color
      // shape is a circle so only one value needs to precess
      final var hue = (m_rainbowFirstPixelHue + (i * 180 / Constants.LEDs.kLEDBufferLength)) % 180;
      // Set the value
      m_LEDBuffer.setHSV(i, hue, 10, 255);
    }
    // Increase by to make the rainbow "move"
    m_rainbowFirstPixelHue += 3;
    // Check bounds
    m_rainbowFirstPixelHue %= 180;   
  } 

  @Override
  public void testInit() {
    // Cancels all running commands at the start of test mode.
    CommandScheduler.getInstance().cancelAll();
  }

  /** This function is called periodically during test mode. */
  @Override
  public void testPeriodic() {}

  /** This function is called once when the robot is first started up. */
  @Override
  public void simulationInit() {}

  /** This function is called periodically whilst in simulation. */
  @Override
  public void simulationPeriodic() {}
}

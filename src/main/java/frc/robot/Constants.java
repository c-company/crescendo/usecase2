// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.geometry.Translation3d;
import swervelib.math.Matter;
import swervelib.parser.PIDFConfig;
import edu.wpi.first.math.util.Units;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide
 * numerical or boolean
 * constants. This class should not be used for any other purpose. All constants
 * should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>
 * It is advised to statically import this class (or one of its inner classes)
 * wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {

  public static final double ROBOT_MASS = (148 - 20.3) * 0.453592; // 32lbs * kg per pound
  public static final Matter CHASSIS = new Matter(new Translation3d(0, 0, Units.inchesToMeters(8)), ROBOT_MASS);
  public static final double LOOP_TIME = 0.13; // s, 20ms + 110ms sprk max velocity lag

  public static final class Auton {

    public static final PIDFConfig TranslationPID = new PIDFConfig(0.7, 0, 0);
    public static final PIDFConfig angleAutoPID = new PIDFConfig(0.4, 0, 0.01);

    public static final double MAX_ACCELERATION = 2;
  }

  public static final class ClimbConstants {

    public static final int kCANIDLEADClimbMotor = 3;
    public static final int kCANIDFollowClimbMotor = 7;
    public static final int kSmartCurrentLimit = 40;
    public static final double kClimbGearRatio = (16 / 1.0); // 16:1
    public static final int kClimbTopPosition = -142; // rotations negative is up (MAC tested on 2/23)
    public static final int kClimbBottomPosition = 15; // rotations (was -30 for half)
    public static final float kClimbSoftLimit = 10; // shouldn't be needed cause of physical limit switch wiring, but
                                                   // just in case of
    public static final double kClimbP = .25;  // was .00005
    public static final double kClimbI = 0; // was .000001
    public static final double kClimbD = 0;  // was .00005
    public static final double kClimbIz = 0;
    public static final double kClimbFF = 0; // was .000156
    public static final double kClimbMaxOutput = 1;
    public static final double kClimbMinOutput = -1;
    public static final double kClimbSpeed = 0.5;
    public static final double kClimberReelInSpeed = 0.1;
    public static final double kClimberOffSpeed = 0;
    public static final double kClimbSmartMaxVelocity = 3000;
    public static final double kClimbSmartMinOutputVelocity = 0;
    public static final double kClimbSmartMaxAccel = 400;
  }

  public static class OperatorConstants {

    // Joystick Deadband
    public static final double LEFT_X_DEADBAND = 0.1;
    public static final double LEFT_Y_DEADBAND = 0.1;
    public static final double RIGHT_X_DEADBAND = 0.05;
    public static final double TURN_CONSTANT = 0.75;
    public static final int kDriverControllerPort = 0; // USB Port for Driver Controller
    public static final int kManipulatorControllerPort = 1; // USB Port for Manipulator Controller
    public static final int kPitControllerPort = 2; //USB Port to unwind motors ONLY USED IN PIT
  }

  public static final class Drivebase {

    // Hold time on motor brakes when disabled
    public static final double WHEEL_LOCK_TIME = 10; // seconds

    // Max swerve speed
    public static final double kMaxSpeed = 14.5; // feet/second //14.5

    // PID for Smart Rotation Control
    public static final PIDController ROTATION_PID_CONTROLLER =
        new PIDController(2, 0, 0);

    // Smart Rotation Angles
    public static final double kRedAmpRot = 270; // reversed 180 due to blue origin
    public static final double kBlueAmpRot = 270;
    public static final double kRedSourceRot = 60; // reversed 180 due to blue origin
    public static final double kBlueSourceRot = 120;
    public static final double kRedSpeakerX = 16.58;
    public static final double kRedSpeakerY = 5.55;
    public static final double kBlueSpeakerX = 0;
    public static final double kBlueSpeakerY = 5.55;
    public static final double kRedPassX = 14.7;
    public static final double kRedPassY = 8;
    public static final double kBluePassX = 1.8;
    public static final double kBluePassY = 8;

  }

  public static final class LimeLightConstants {
    public static final String kLimelightName = "limelight";
    public static final int kAllAprilTagPipeline = 0;
    public static final int kSpeakerPipeline = 1;
    public static final double kHeightOfLimelightInch = 25.5;
    public static final int kVerticleOffsetLimelightmountDegrees = 30;
    public static final double kSpeakerBestHeightInchs = 80; //Estimate
    public static final double kLimeLightToHinge = 0.698;
    public static final double kHingeToShooter = 4.75;
  }

  // Constants for IntakeShooterSubsystem
  public static final class IntakeShooterSubsystemConstants {

    /*
     * NOTES
     * Left and Right are relative to the robot front, in our configuration that is
     * the oppsite of our speaker shooter direction
     * i.e. back of the robot is where it gets shot out of in auto.
     * The set up of all motors drives a positive voltage value move the note from
     * the ground intake
     * out toward the back of the robot. The inversions below are so that if we turn
     * the motors on positive it will
     * flow toward the back of the robot
     */

    // *****INITIALIZATION OF MOTORS*******
    // motor left shooter configuration.
    // This is the motor that shoots the note into the speaker and amp,
    // but also intakes it from the source. For intake it needs to be set to a
    // negative value.
    public static final int kCANIDLeftShooterMotor = 16;
    public static final boolean kLeftShooterInvert = false;

    // motor right shooter configuration.
    // This is the motor that shoots the note into the speaker and amp,
    // but also intakes it from the source. For intake it needs to be set to a
    // negative value.
    public static final int kCANIDRightShooterMotor = 15;
    public static final boolean kRightShooterInvert = true;

    // motor hop up configuration.
    // This is the motor that engages the shooter wheeels for sending the note out
    // the back of the robot with a positive voltage value.
    // when using the trap configuratin this motor will need a negative value
    public static final int kCANIDHopupShooterMotor = 14;
    public static final boolean kHopupInvert = false;

    // motor catcher configuration.
    // This is the motor that catches the note from the intake flipper subassembly
    // when using the trap configuratin this motor will need a negative value
    public static final int kCANIDShooterCatcherMotor = 13;
    public static final boolean kShooterCatcherMotorInvert = true;

    // motor angle configuration
    // This is the motor is what angles the intake/shooter
    // when moving down into the robot base this will be a negative value - it is
    // wired using a reverse limit switch
    // this is why the motor is inverted
    public static final int kCANIDShooterAngleMotor = 17;
    public static final boolean kShooterAngleMotorInvert = true;
    public static final double KShooterGearRatio = (62.5 / 1.0); // 62.5 :1
    public static final float kShootrerAngleSoftLimit = 30; // bsangel about 170.5

    // FLIPPER MOTOR CONFIG

    // motor top intake grabber wheel
    // TODO determine inversion
    public static final int kCANIDTopFloorIntakeMotor = 4;
    public static final boolean kTopFloorIntakeMotorInvert = false;

    // motor bottom intake grabber wheel
    // TODO determine inversion
    public static final int kCANIDBottomFloorIntakeMotor = 6;
    public static final boolean kBottomFloorIntakeMotorInvert = false;

    // motor angle flipper
    // mac-tested to work with Reverse limit switch
    public static final int kCANIDFlipperMotor = 5;
    public static final boolean kFlipperMotorInvert = false;
    public static final double KFlipperGearRatio = (125 / 1.0); // 125 :1

    // motor intake conveyer
    // TODO determine inversion
    public static final int kCANIDIntakeConveyer = 12;
    public static final boolean kIntakeConveyerInvert = true;

    // IntakeShooter Sensors Initialization - the Side is the placement of the LED
    // break sensor
    public static final int kSensorShooterSide = 0;
    public static final int kSensorFloorIntakeSide = 1;

    // Shooter Left Motor PID Variables
    // public static final double kShooterLeftMotorP =0.00036; //39
    // public static final double kShooterLeftMotorI = 0;
    // public static final double kShooterLeftMotorD = .00027; // 25
    // public static final double kShooterLeftMotorz = 0;
    // public static final double kShooterLeftMotorFF = 0.00018;//17
    // public static final double kShooterLeftMotorMaxOutput = 1;
    // public static final double kShooterLeftMotorMinOutput = -1;

    // Shoot Right Motor PID Variables
    // public static final double kShooterRightMotorP = 0.00036; //39
    // public static final double kShooterRightMotorI = 0;
    // public static final double kShooterRightMotorD = .00027; //25
    // public static final double kShooterRightMotorz = 0;
    // public static final double kShooterRightMotorFF = 0.00018;//17
    // public static final double kShooterRightMotorMaxOutput = 1;
    // public static final double kShooterRightMotorMinOutput = -1;

    // Shoot Angle PID Variables
    public static final double kShooterAngleP = .0001;
    public static final double kShooterAngleI = .00000;//was .000001
    public static final double kShooterAngleD = .00003;// .00005;
    public static final double kShooterAnglez = 0;
    public static final double kShooterAngleFF = 0.000156;
    public static final double kShooterAnglerMaxOutput = 1;
    public static final double kShooterAnglerMinOutput = -1;
    public static final double kSmartMotionMaxVelocity = 5500; // rpms
    public static final double kSmartMotionMinOutputVelocity = 0; // rpms
    public static final double kSmartMotionMaxAccel = 4500; // rpms

    // Flipper PID Variables
    public static final double kFlipperP = .25;
    public static final double kFlipperI = 0;
    public static final double kFlipperD = 0;
    public static final double kFlipperz = 0;
    public static final double kFlipperFF = 0;
    public static final double kFlipperMaxOutput = 1;
    public static final double kFlipperMinOutput = -1;

    // Ground Top Intake PID Variables
    // public static final double kGrountIntakeTopP = .25;
    // public static final double kGrountIntakeTopI = 0;
    // public static final double kGrountIntakeTopD = 0;
    // public static final double kGrountIntakeTopz = 0;
    // public static final double kGrountIntakeTopFF = 0;
    // public static final double kGrountIntakeTopMaxOutput = 1;
    // public static final double kGrountIntakeTopMinOutput = -1;

    // Ground Bottom Intake PID Variables
    // public static final double kGrountIntakeBottomP = .25;
    // public static final double kGrountIntakeBottomI = 0;
    // public static final double kGrountIntakeBottomD = 0;
    // public static final double kGrountIntakeBottomz = 0;
    // public static final double kGrountIntakeBottomFF = 0;
    // public static final double kGrountIntakeBottomMaxOutput = 1;
    // public static final double kGrountIntakeBottomMinOutput = -1;

    // *** SHOOT/INTAKE CONFIGURATION *****

    // initialization configurations
     public static final double kShooterAngleOffset = 3.25;
     public static final double kShooterWheelToHingeAngle = 20;
     public static final double kMotorOffSpeed = 0;
     public static final double kShooterAngleToleranceRotations = 0.15; // was 0.085; -> 1/4 degree tolerance in rotations
     public static final double kShooterAngleToleranceDegrees = 0.1; //degrees
     public static final double kRotationTolerance = 2.5;
     public static final double kShooterReelInFlipperSpeed = -0.1; //For Pit Controller

    // TRAP configuration
    public static final double kShooterRotationsTrap = 29.49; // bsangle 170
    public static final double kShooterRotationsPreClimb = 0; 
    public static final double kTrapScoreReversePercentage = -0.15;
    public static final double kTrapScoreHopUpReversePercentage = -1;
    public static final double kTrapCatcherPercentage = 1;
    

    // Pickup from source configuration
    public static final double kShooterRotationsSource = 7.76; // bs angle 44
    public static final double kShooterPickupFromSourceSpeed = -0.35;
    public static final double kHopupPickupFromSourceSpeed = -0.2;
    public static final double kCatcherPickupFromSourceSpeed = -0.2;

    // Pickup from floor configuration
    public static final double kShooterRotationsGroundPickup = 5.9; // bs angle 34
    public static final double kFlipperDeployPosition = 25;
    public static final double kFlipperExpelPosition = 15;
    public static final double kFlipperStowPosition = 7.5; // was 6
    public static final double kFlipperExpelSpeed = -1;
    public static final double kHopupPickupFromFloorSpeed = 0.35;
    public static final double kCatcherPickupFromFloorSpeed = 0.35;
    public static final double kIntakeTopFloorMotorSpeed = 1.0;
    public static final double kIntakeBottomFloorMotorSpeed = 1.0;
    public static final double kGroundIntakeConveyorMotorSpeed = 1.0;
    public static final double kgroundHopUpReverseSpeed = -0.3;
    public static final double kgroundCatcherReverseSpeed = -0.3;
    public static final double ksourceReverseSpeed = 0.3;

    // amp configuration
    public static final double kShooterRotationsAmp = 22;
    public static final double kLeftShooterAmpPercentage = 0.5;
    public static final double kRightShooterAmpPercentage = 0.5;
    public static final double kHopUpAmpPercentage = 0.5;
    public static final double kCatcherAmpPercentage = 0.5;

    // stowed configuration
    public static final double kShooterRotationsStowed = 5.9; // bs angle 34

    // Speaker configuration
    public static final double kShooterRotationsManualSpeaker = 5.5; // (for Dumbshoot)
    public static final double kShootMotorFullSpeed = 1; // for Dumbshooter
    public static final double kMotorFullSpeed = 5400; // used for RPM check before firing
    public static final double kHopupSpeakerSpeed = 1; // for Smartshoot
    public static final double kCatcherSpeakerSpeed = 1; // for Smartshoot
    public static final double kNoteSpeedInches = 0.00508; // TODO placeholder

    // PassNote configuration
    public static final double kShooterAnglePassNote = 70; // Shooter angle for PassNote
  }

  public static final class LEDs {
    /* Led Constants */
    public static final int kPwmChannel = 0; // TODO - verify after wiring
    public static final int kLength = 120; // TODO - verify after wiring
    public static final int kLEDBufferLength = 120; // TODO - verify after wiring
    public static final int kPurple = 215;
    public static final int kGreen = 60;
    public static final int kBlue = 125;
    public static final int kYellow = 10;
    public static final int kWhite = 255;
    public static final int kRed = 0;
    public static final int kPink = 351;
    public static final int kSat0 = 0;
    public static final int kSatDefault = 255;
    public static final int kConeModeColor = 10;
    public static final int kCubeModeColor = 125;
    public static final int underbot_lights = 0; // placeholder
    public static final int support_lights = 9;
  }

}

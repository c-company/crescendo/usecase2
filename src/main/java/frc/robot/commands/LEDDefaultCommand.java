// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.
// led default command

package frc.robot.commands;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
//import edu.wpi.first.math.MathUtil;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Constants;
import frc.robot.subsystems.LEDSubsystem;
import frc.robot.subsystems.RainbowSubsytem;
import frc.robot.subsystems.LimelightHelpers;
import frc.robot.Constants.LEDs;
import frc.robot.Constants.LimeLightConstants;

public class LEDDefaultCommand extends Command {
  /** Creates counter new LimelightDefaultCommand. */
  private LEDSubsystem m_LED;
  private RainbowSubsytem m_RainbowMode;
  
  
    public LEDDefaultCommand(LEDSubsystem s_LED, RainbowSubsytem s_RainbowMode) {
    // Use addRequirements() here to declare subsystem dependencies.
    m_LED = s_LED;     
    m_RainbowMode = s_RainbowMode;
    addRequirements(m_LED,m_RainbowMode);  
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  //  m_LED.setModeColor(Constants.LEDs.kYellow, LEDs.kSatDefault); // for cone on start up
    //m_LED.setTargetColor(Constants.LEDs.kRed, LEDs.kSatDefault);  // for I can't see the target on start up
   // m_LED.startLEDs();
  

  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    
  }
  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
   // m_LED.stopLEDs();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}

// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.IntakeShooterSubsystem;
import frc.robot.Constants.IntakeShooterSubsystemConstants;

public class TrapShooter extends Command {
  private IntakeShooterSubsystem m_IntakeShooterSubsystem;
  private int m_finishCount;
  
  /** Creates a new TrapShooter. */
  public TrapShooter(IntakeShooterSubsystem p_IntakeShooterSubsystem) {
    // Use addRequirements() here to declare subsystem dependencies.
   addRequirements(p_IntakeShooterSubsystem);
   m_IntakeShooterSubsystem = p_IntakeShooterSubsystem;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {

    m_finishCount = 0;
    // catcher and hop-up motors running in reverse
    m_IntakeShooterSubsystem.setHopupPercentage(IntakeShooterSubsystemConstants.kTrapScoreReversePercentage);
    m_IntakeShooterSubsystem.setCatcherPercentage(IntakeShooterSubsystemConstants.kTrapScoreReversePercentage);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {}

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    // catcher motor and Hopup Motor shut down
    m_IntakeShooterSubsystem.disableHopupMotor();
    m_IntakeShooterSubsystem.disableCatcherMotor();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    // check if intake from ground sensor is blocked?
    if (!(m_IntakeShooterSubsystem.getIntakeFromGroundSensorBlocked()))
      m_finishCount++;

  if(m_finishCount >= 3)
    return true;
  else 
    return false;
    
  }
}

// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Constants.IntakeShooterSubsystemConstants;
import frc.robot.subsystems.IntakeShooterSubsystem;

public class PassNote extends Command {
  private IntakeShooterSubsystem m_shooter;
  private int m_finishCount;
  private int m_fullSpeedCount;

  /** Creates a new PassNote. */
  public PassNote(IntakeShooterSubsystem p_shooter) {
    m_shooter = p_shooter;

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(p_shooter);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {

    // turn shooter wheels on
    m_shooter.setShooterWheelsSpeed(IntakeShooterSubsystemConstants.kShootMotorFullSpeed,
        IntakeShooterSubsystemConstants.kShootMotorFullSpeed);

    m_shooter.setShooterSetPoint(true, IntakeShooterSubsystemConstants.kShooterAnglePassNote);

    // initialize counters
    m_finishCount = 0;
    m_fullSpeedCount = 0;

  } // end initialize

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {

    // if Shooter angle is within tolerance
      if ((Math.abs(m_shooter.GetShooterAngleDegree())
          - IntakeShooterSubsystemConstants.kShooterAnglePassNote) <= IntakeShooterSubsystemConstants.kShooterAngleToleranceDegrees) { // if1
        // if shooter is running full speed
        if (m_shooter.checkShooterFullSpeed() || (m_fullSpeedCount >= 20)) { // if2
          m_shooter.setHopupPercentage(IntakeShooterSubsystemConstants.kHopupSpeakerSpeed);
          m_shooter.setCatcherPercentage(IntakeShooterSubsystemConstants.kShootMotorFullSpeed);
        } // end if2
        m_fullSpeedCount++;
      } // end if1

  } // end execute

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {

    // turn off motors
    m_shooter.setShooterWheelsSpeed(IntakeShooterSubsystemConstants.kMotorOffSpeed,
        IntakeShooterSubsystemConstants.kMotorOffSpeed);
    m_shooter.disableHopupMotor();
    m_shooter.setCatcherPercentage(IntakeShooterSubsystemConstants.kMotorOffSpeed);

    // stow shooter
    m_shooter.setShooterToStowed();

  } // end end

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {

    // wait for 0.5 seconds after both sensors are clear before ending
    if (!(m_shooter.getIntakeFromGroundSensorBlocked()) && !(m_shooter.getIntakeFromSourceSensorBlocked()))
      m_finishCount++;

    if (m_finishCount == 3)
      return true;
    else
      return false;

  } // end isFinished

} // End TrackAndShoot

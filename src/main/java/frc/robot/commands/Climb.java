// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.ClimbSubsystem;
import frc.robot.subsystems.IntakeShooterSubsystem;

public class Climb extends Command {
  
  private ClimbSubsystem m_ClimbSubsystem; 
  private IntakeShooterSubsystem m_IntakeShooterSubsystem;
  
  /** Creates a new Climb. */
  public Climb(ClimbSubsystem p_ClimbSubsystem, IntakeShooterSubsystem p_ShooterSubsystem) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(p_ClimbSubsystem, p_ShooterSubsystem);
    m_ClimbSubsystem = p_ClimbSubsystem;
    m_IntakeShooterSubsystem = p_ShooterSubsystem;
  }
    

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    // initiate climb
    m_ClimbSubsystem.LowerClimber();
    // TODO - need to wait for climb to complete?
    // Setting Shooter Position to trap pos//
    m_IntakeShooterSubsystem.setShooterToTrap();

  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    // not sure we need to do anything here?
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    // set shooter to travel?
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    // is shoot complete?
    return false;
  }
}

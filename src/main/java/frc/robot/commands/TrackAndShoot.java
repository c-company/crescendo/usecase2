// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import java.util.function.BooleanSupplier;
import java.util.function.DoubleSupplier;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Constants.Drivebase;
import frc.robot.Constants.IntakeShooterSubsystemConstants;
import frc.robot.Constants.LimeLightConstants;
import frc.robot.subsystems.IntakeShooterSubsystem;
import frc.robot.subsystems.LimelightHelpers;
import frc.robot.subsystems.swervedrive.SwerveSubsystem;

public class TrackAndShoot extends Command {
  private SwerveSubsystem m_swerve;
  private IntakeShooterSubsystem m_shooter;
  private DoubleSupplier m_translationX;
  private DoubleSupplier m_translationY;
  private PIDController m_rotationController;
  private BooleanSupplier m_leftTrigger;
  private double m_rotationError;
  private double m_rotationSetpoint; // in degrees
  private double m_distance;
  private double m_speakerSetpoint;
  private double m_customAngleOffset;
  private int m_finishCount;
  private int m_shooterSettleCount;
  private int m_fullSpeedCount;
  private boolean m_isRedAlliance;
  private double m_rotationAngle;

  /** Creates a new TrackAndShoot. */
  public TrackAndShoot(SwerveSubsystem p_swerve, IntakeShooterSubsystem p_shooter, boolean p_isRedAlliance,
      DoubleSupplier p_translationX, DoubleSupplier p_translationY, BooleanSupplier p_leftTrigger) {
    m_swerve = p_swerve;
    m_shooter = p_shooter;
    m_isRedAlliance = p_isRedAlliance;
    m_translationX = p_translationX;
    m_translationY = p_translationY;
    m_leftTrigger = p_leftTrigger;
    m_rotationController = Drivebase.ROTATION_PID_CONTROLLER;

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(p_swerve, p_shooter);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {

    // set LL Pipeline to Speaker
    LimelightHelpers.setPipelineIndex(LimeLightConstants.kLimelightName, LimeLightConstants.kSpeakerPipeline);
    LimelightHelpers.setLEDMode_ForceOff(LimeLightConstants.kLimelightName);

    // turn shooter wheels on
    m_shooter.setShooterWheelsSpeed(IntakeShooterSubsystemConstants.kShootMotorFullSpeed,
        IntakeShooterSubsystemConstants.kShootMotorFullSpeed);

    // Set up rotational PID
    m_rotationController.enableContinuousInput(-Math.PI, Math.PI);
    m_rotationController.reset();

    // initialize counters
    m_finishCount = 0;
    m_shooterSettleCount = 0;
    m_fullSpeedCount = 0;

  }  // end initialize


  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {

    // Search for target (pipeline filtered to speaker IDs)
    if (LimelightHelpers.getTV(LimeLightConstants.kLimelightName) == false) {

      // Find angle to speaker
      if (m_isRedAlliance){ // if Red
        m_rotationAngle = 180 + Math.atan((m_swerve.getPose().getY() - Drivebase.kRedSpeakerY) /
              (Drivebase.kRedSpeakerX - m_swerve.getPose().getX()));
      }
      else { // if Blue
        m_rotationAngle = Math.atan((Drivebase.kBlueSpeakerY - m_swerve.getPose().getY()) /
              m_swerve.getPose().getX());
      }
      
      // Set rotation setpoint to point towards Speaker
      m_rotationController.setSetpoint(Math.toRadians(m_rotationAngle));

      // Get current rotation
      m_rotationError = m_swerve.getPose().getRotation().getRadians();

      // Drive with rotation set to 0 until target is found
      m_swerve.drive(new Translation2d((Math.pow(m_translationY.getAsDouble(), 3) * m_swerve.getMaxVelocity()),
          Math.pow(m_translationX.getAsDouble(), 3) * m_swerve.getMaxVelocity()),
          m_rotationController.calculate(m_rotationError), true);

    } // end if

    if (LimelightHelpers.getTV(LimeLightConstants.kLimelightName) == true) {

      // Get rotation setpoint to Speaker in degrees
      m_rotationSetpoint = m_swerve.getPose().getRotation().getDegrees()
          - LimelightHelpers.getTX(LimeLightConstants.kLimelightName);

      // Set Rotation PID setpoint
      m_rotationController.setSetpoint(Math.toRadians(m_rotationSetpoint));

      // Get current rotation
      m_rotationError = m_swerve.getPose().getRotation().getRadians();

      // Drive with Speaker tracking
      m_swerve.drive(new Translation2d((Math.pow(m_translationY.getAsDouble(), 3) * m_swerve.getMaxVelocity()),
          Math.pow(m_translationX.getAsDouble(), 3) * m_swerve.getMaxVelocity()),
          m_rotationController.calculate(m_rotationError), true);

      // Get Distance to target
      m_distance = m_shooter.GetSpeakerDistance();

      // Get custom offset for distance
      m_customAngleOffset = m_shooter.calculateAngleOffset(m_distance);

      // calculate shooter setpoint with offset
      m_speakerSetpoint = 90 - IntakeShooterSubsystemConstants.kShooterAngleOffset - m_customAngleOffset
          - m_shooter.getSpeakerAngle();

      if(m_speakerSetpoint >= 90){
        m_speakerSetpoint = 80;
      }

      if(m_speakerSetpoint < 10){
        m_speakerSetpoint = 10;
      }

      // set shooter angle setpoint
      m_shooter.setShooterSetPoint(true, m_speakerSetpoint);

      // when left manipulator trigger is true
      if (m_leftTrigger.getAsBoolean() == true) { // if1
        if (m_shooter.checkShooterFullSpeed() || (m_fullSpeedCount >= 25)) { // if2
          if ((Math.abs(m_shooter.GetShooterAngleDegree())
              - m_speakerSetpoint) <= IntakeShooterSubsystemConstants.kShooterAngleToleranceDegrees) { // if3
            if (m_shooterSettleCount == 5) { // if4
              m_shooter.setHopupPercentage(IntakeShooterSubsystemConstants.kHopupSpeakerSpeed);
              m_shooter.setCatcherPercentage(IntakeShooterSubsystemConstants.kShootMotorFullSpeed);
            } // end if4

            m_shooterSettleCount++;
          } // end if3
        } // end if2

        m_fullSpeedCount++;
      } // if1

    } // end if
  } // end execute


  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {

    // turn off motors
    m_shooter.setShooterWheelsSpeed(IntakeShooterSubsystemConstants.kMotorOffSpeed,
        IntakeShooterSubsystemConstants.kMotorOffSpeed);
    m_shooter.disableHopupMotor();
    m_shooter.setCatcherPercentage(IntakeShooterSubsystemConstants.kMotorOffSpeed);

    // stow shooter
    m_shooter.setShooterToStowed();

    // set LL back to all tags pipeline
    LimelightHelpers.setPipelineIndex(LimeLightConstants.kLimelightName, LimeLightConstants.kAllAprilTagPipeline);
    LimelightHelpers.setLEDMode_ForceOff(LimeLightConstants.kLimelightName);
    
  }  // end end

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {

    // wait for 0.5 seconds after both sensors are clear before ending
    if (!(m_shooter.getIntakeFromGroundSensorBlocked()) && !(m_shooter.getIntakeFromSourceSensorBlocked()))
      m_finishCount++;

    if (m_finishCount == 3)
      return true;
    else
      return false;

  } // end isFinished

}  // End TrackAndShoot

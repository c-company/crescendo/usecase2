// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.subsystems.IntakeShooterSubsystem;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class TrapShooterCommandGroup extends SequentialCommandGroup {
  IntakeShooterSubsystem m_IntakeShooterSubsystem;
  
  /** Creates a new TrapShooterCommandGroup. */
  public TrapShooterCommandGroup(IntakeShooterSubsystem p_IntakeShooterSubsystem) {
    m_IntakeShooterSubsystem = p_IntakeShooterSubsystem;
    // Add your commands in the addCommands() call, e.g.
    // addCommands(new FooCommand(), new BarCommand());
    addCommands(
      m_IntakeShooterSubsystem.runOnce(() -> m_IntakeShooterSubsystem.setShooterToTrap()),
      new WaitCommand(3.5), //TODO: validate timing//was 3
      new TrapShooter(m_IntakeShooterSubsystem),
      new TrapShooterReverse(m_IntakeShooterSubsystem));
    }
  }

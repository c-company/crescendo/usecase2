// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.IntakeShooterSubsystem;
import frc.robot.Constants.IntakeShooterSubsystemConstants;
import frc.robot.subsystems.LimelightHelpers;
import frc.robot.Constants.LimeLightConstants;

public class SmartShootSpeaker extends Command {
  private IntakeShooterSubsystem m_IntakeShooterSubsystem;
  private int m_finishCount;
  private int m_counter;

  /** Creates a new ShootSpeaker. */
  public SmartShootSpeaker(IntakeShooterSubsystem p_IntakeShooterSubsystem) {
    addRequirements(p_IntakeShooterSubsystem);
    m_IntakeShooterSubsystem = p_IntakeShooterSubsystem;

    // Use addRequirements() here to declare subsystem dependencies.
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {

    m_finishCount = 0;
    m_counter = 0;

    //change to Speaker Pipeline
    LimelightHelpers.setPipelineIndex(LimeLightConstants.kLimelightName, LimeLightConstants.kSpeakerPipeline);

    // turn on shooter motors
    m_IntakeShooterSubsystem.setShooterWheelsSpeed(IntakeShooterSubsystemConstants.kShootMotorFullSpeed,
        IntakeShooterSubsystemConstants.kShootMotorFullSpeed);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {

    // set shooter angle
    m_IntakeShooterSubsystem.setShooterSetPoint(true, 90 - m_IntakeShooterSubsystem.GetSpeakerAngleV2());

    if (m_IntakeShooterSubsystem.checkShooterFullSpeed() || m_counter == 50) {
      if (Math.abs(m_IntakeShooterSubsystem.GetShooterAngleDegree()) - (90 - m_IntakeShooterSubsystem
          .GetSpeakerAngleV2()) <= IntakeShooterSubsystemConstants.kShooterAngleToleranceDegrees) {
        m_IntakeShooterSubsystem.setHopupPercentage(IntakeShooterSubsystemConstants.kHopupSpeakerSpeed);
        m_IntakeShooterSubsystem.setCatcherPercentage(IntakeShooterSubsystemConstants.kShootMotorFullSpeed);
      }
    }
    
    m_counter++;

  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {

    // turn off motors
    m_IntakeShooterSubsystem.setShooterWheelsSpeed(IntakeShooterSubsystemConstants.kMotorOffSpeed,
        IntakeShooterSubsystemConstants.kMotorOffSpeed);
    m_IntakeShooterSubsystem.disableHopupMotor();
    m_IntakeShooterSubsystem.setCatcherPercentage(IntakeShooterSubsystemConstants.kMotorOffSpeed);

    // set shooter to stowed position
    m_IntakeShooterSubsystem.setShooterToStowed();

    //set LL Pipeline back to AllTags
    LimelightHelpers.setPipelineIndex(LimeLightConstants.kLimelightName, LimeLightConstants.kAllAprilTagPipeline);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    if (!(m_IntakeShooterSubsystem.getIntakeFromGroundSensorBlocked()) && !(m_IntakeShooterSubsystem.getIntakeFromSourceSensorBlocked()))
      m_finishCount++;

    if (m_finishCount == 25)
      return true;
    else
      return false;
  }
}

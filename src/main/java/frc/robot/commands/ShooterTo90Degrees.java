// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.IntakeShooterSubsystem;
import frc.robot.Constants.IntakeShooterSubsystemConstants;



public class ShooterTo90Degrees extends Command {
  private IntakeShooterSubsystem m_IntakeShooterSubsystem;
  /** Creates a new ShooterTo90Digres. */
  public ShooterTo90Degrees(IntakeShooterSubsystem p_IntakeShooterSubsystem) {
    addRequirements(p_IntakeShooterSubsystem);
    m_IntakeShooterSubsystem = p_IntakeShooterSubsystem;
    // Use addRequirements() here to declare subsystem dependencies.
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    m_IntakeShooterSubsystem.setShooterSetPoint(true,(90 - IntakeShooterSubsystemConstants.kShooterAngleOffset)) ; 

    m_IntakeShooterSubsystem.setShooterWheelsSpeed(IntakeShooterSubsystemConstants.kShootMotorFullSpeed,
      IntakeShooterSubsystemConstants.kShootMotorFullSpeed);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
   if (m_IntakeShooterSubsystem.checkShooterFullSpeed()){
    if(Math.abs(m_IntakeShooterSubsystem.GetShooterAngleDegree() - (90 - IntakeShooterSubsystemConstants.kShooterAngleOffset)) <= 
      IntakeShooterSubsystemConstants.kShooterAngleToleranceDegrees) {
       m_IntakeShooterSubsystem.setHopupPercentage(IntakeShooterSubsystemConstants.kHopupSpeakerSpeed);
       m_IntakeShooterSubsystem.setCatcherPercentage(IntakeShooterSubsystemConstants.kShootMotorFullSpeed);
    }
  }
}

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    m_IntakeShooterSubsystem.setShooterWheelsSpeed(IntakeShooterSubsystemConstants.kMotorOffSpeed,
    IntakeShooterSubsystemConstants.kMotorOffSpeed);

    m_IntakeShooterSubsystem.disableHopupMotor();

    m_IntakeShooterSubsystem.setCatcherPercentage(IntakeShooterSubsystemConstants.kMotorOffSpeed);

    m_IntakeShooterSubsystem.setShooterToStowed();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
  return !m_IntakeShooterSubsystem.getIntakeFromGroundSensorBlocked();
  }
}

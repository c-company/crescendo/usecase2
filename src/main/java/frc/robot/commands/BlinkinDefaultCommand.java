// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.
// led default command

package frc.robot.commands;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
//import edu.wpi.first.math.MathUtil;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Constants;
import frc.robot.subsystems.BlinkSubSystem;
import frc.robot.subsystems.LEDSubsystem;
import frc.robot.subsystems.RainbowSubsytem;
import frc.robot.subsystems.LimelightHelpers;
import frc.robot.Constants.LEDs;
import frc.robot.Constants.LimeLightConstants;

public class BlinkinDefaultCommand extends Command {
  /** Creates counter new LimelightDefaultCommand. */
    private BlinkSubSystem m_blinkin_support_lights;   
    public BlinkinDefaultCommand(BlinkSubSystem p_blinkin_support_lights) {
    // Use addRequirements() here to declare subsystem dependencies.
     m_blinkin_support_lights = p_blinkin_support_lights;     
     addRequirements(m_blinkin_support_lights);  
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  

  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {

    /// get has target
    // if has target check for ID 4 or 7
    //
    
  }
  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
 
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}

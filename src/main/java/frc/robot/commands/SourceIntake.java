// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.BlinkSubSystem;
import frc.robot.subsystems.IntakeShooterSubsystem;
import frc.robot.subsystems.LimelightHelpers;
import frc.robot.Constants.IntakeShooterSubsystemConstants;
import frc.robot.Constants.LimeLightConstants;
import edu.wpi.first.wpilibj.Servo;


public class SourceIntake extends Command {
  IntakeShooterSubsystem m_Shooter_Intake;
  int m_endTimer;

  ///private BlinkSubSystem m_Servo;
  /** Creates a new Intake. */
  public SourceIntake(IntakeShooterSubsystem p_Shooter_Intake) {
    m_Shooter_Intake = p_Shooter_Intake;
    //m_Servo = p_Servo;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(m_Shooter_Intake);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    m_Shooter_Intake.setShooterToSourcePickup();
    m_Shooter_Intake.setShooterWheelsSpeed(IntakeShooterSubsystemConstants.kShooterPickupFromSourceSpeed, IntakeShooterSubsystemConstants.kShooterPickupFromSourceSpeed);
    m_Shooter_Intake.setHopupPercentage(IntakeShooterSubsystemConstants.kHopupPickupFromSourceSpeed);
    m_Shooter_Intake.setCatcherPercentage(IntakeShooterSubsystemConstants.kCatcherPickupFromSourceSpeed);
    LimelightHelpers.setLEDMode_ForceOn(LimeLightConstants.kLimelightName);
    LimelightHelpers.setLEDMode_ForceBlink(LimeLightConstants.kLimelightName);

    m_endTimer = 0;
  }
  
    // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {}

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {

    while (!m_Shooter_Intake.getIntakeFromSourceSensorBlocked() && (m_endTimer <= 100000)) {
      m_endTimer++;
    }

    while(!m_Shooter_Intake.getIntakeFromGroundSensorBlocked() && (m_endTimer <= 100000)){
      m_Shooter_Intake.setCatcherPercentage(IntakeShooterSubsystemConstants.ksourceReverseSpeed);
      m_Shooter_Intake.setHopupPercentage(IntakeShooterSubsystemConstants.ksourceReverseSpeed);
      m_endTimer++;
    }
    while(!m_Shooter_Intake.getIntakeFromSourceSensorBlocked() && (m_endTimer <= 100000)){
      m_Shooter_Intake.setCatcherPercentage(IntakeShooterSubsystemConstants.kgroundHopUpReverseSpeed);
      m_Shooter_Intake.setHopupPercentage(IntakeShooterSubsystemConstants.kgroundHopUpReverseSpeed);
      m_endTimer++;
    }
   m_Shooter_Intake.setShooterWheelsSpeed(IntakeShooterSubsystemConstants.kMotorOffSpeed, IntakeShooterSubsystemConstants.kMotorOffSpeed);
   m_Shooter_Intake.setHopupPercentage(IntakeShooterSubsystemConstants.kMotorOffSpeed);
   m_Shooter_Intake.setCatcherPercentage(IntakeShooterSubsystemConstants.kMotorOffSpeed);
   m_Shooter_Intake.setShooterToStowed();
   
   ///m_Servo.Blink();
  LimelightHelpers.setLEDMode_ForceOff(LimeLightConstants.kLimelightName);
   
 
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return m_Shooter_Intake.getIntakeFromSourceSensorBlocked();
  }
}

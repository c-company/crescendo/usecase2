// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.IntakeShooterSubsystem;
import frc.robot.Constants.IntakeShooterSubsystemConstants;

public class IntakeExpeller extends Command {
  private IntakeShooterSubsystem m_IntakeShooterSubsystem;

  /** Creates a new FloorPickup. */
  public IntakeExpeller(IntakeShooterSubsystem p_IntakeShooterSubsystem) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(p_IntakeShooterSubsystem);
    m_IntakeShooterSubsystem = p_IntakeShooterSubsystem;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {

    m_IntakeShooterSubsystem.setIntakeFlipperRotations(false, 
      IntakeShooterSubsystemConstants.kFlipperExpelPosition);

    m_IntakeShooterSubsystem.setConveyerMotorSpeed(IntakeShooterSubsystemConstants.kFlipperExpelSpeed);
    m_IntakeShooterSubsystem.setTopFlipperMotorSpeed(IntakeShooterSubsystemConstants.kFlipperExpelSpeed);
    m_IntakeShooterSubsystem.setBottomFlipperMotorSpeed(IntakeShooterSubsystemConstants.kFlipperExpelSpeed);
    
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    m_IntakeShooterSubsystem.disableIntakeFlipperMotors();
    m_IntakeShooterSubsystem.setFlipperToStow();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}

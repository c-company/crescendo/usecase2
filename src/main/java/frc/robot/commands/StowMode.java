// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Constants.IntakeShooterSubsystemConstants;
import frc.robot.subsystems.IntakeShooterSubsystem;

public class StowMode extends Command {
  IntakeShooterSubsystem m_shooter;

  /** Creates a new StowShooter. */
  public StowMode(IntakeShooterSubsystem p_shooter) {
    m_shooter = p_shooter;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(p_shooter);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    //turn off all motors
    m_shooter.setShooterWheelsSpeed(IntakeShooterSubsystemConstants.kMotorOffSpeed, IntakeShooterSubsystemConstants.kMotorOffSpeed);
    m_shooter.disableCatcherMotor();
    m_shooter.disableHopupMotor();
    m_shooter.disableIntakeFlipperMotors();

    //stow flipper and shooter
    m_shooter.setFlipperToStow();
    m_shooter.setShooterToStowed();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {}

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return true;
  }
}

// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.IntakeShooterSubsystem;

public class FlipperReelIn extends Command {
  IntakeShooterSubsystem m_Shooter;

  /** Creates a new FlipperReelIn. */
  public FlipperReelIn(IntakeShooterSubsystem p_Shooter) {
    m_Shooter = p_Shooter;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(p_Shooter);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    m_Shooter.ReelInFlipper();
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    m_Shooter.FlipperOff();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}

// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.IntakeShooterSubsystem;
import frc.robot.Constants.IntakeShooterSubsystemConstants;

public class FloorPickup extends Command {
  private IntakeShooterSubsystem m_IntakeShooterSubsystem;
  private int m_endTimer;

  /** Creates a new FloorPickup. */
  public FloorPickup(IntakeShooterSubsystem p_IntakeShooterSubsystem) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(p_IntakeShooterSubsystem);
    m_IntakeShooterSubsystem = p_IntakeShooterSubsystem;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    m_IntakeShooterSubsystem.setShooterToFloorPickup();
    m_IntakeShooterSubsystem.setFlipperToDeployed();
    m_IntakeShooterSubsystem.enableIntakeFlipperMotors();
    m_IntakeShooterSubsystem.setCatcherPercentage(IntakeShooterSubsystemConstants.kCatcherPickupFromFloorSpeed);
    m_IntakeShooterSubsystem.setHopupPercentage(IntakeShooterSubsystemConstants.kHopupPickupFromFloorSpeed);

    m_endTimer = 0;
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {

    while (!m_IntakeShooterSubsystem.getIntakeFromGroundSensorBlocked() && (m_endTimer <= 100000)) {
      m_endTimer++;
    }

      while (!m_IntakeShooterSubsystem.getIntakeFromSourceSensorBlocked() && m_endTimer <= 100000) {
        m_IntakeShooterSubsystem.setCatcherPercentage(IntakeShooterSubsystemConstants.kgroundCatcherReverseSpeed);
        m_IntakeShooterSubsystem.setHopupPercentage(IntakeShooterSubsystemConstants.kgroundHopUpReverseSpeed);
        m_endTimer++;
      }
    

    m_IntakeShooterSubsystem.disableIntakeFlipperMotors();
    m_IntakeShooterSubsystem.disableHopupMotor();
    m_IntakeShooterSubsystem.disableCatcherMotor();
    m_IntakeShooterSubsystem.setShooterToStowed();
    m_IntakeShooterSubsystem.setFlipperToStow();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return m_IntakeShooterSubsystem.getIntakeFromGroundSensorBlocked();
  }
}

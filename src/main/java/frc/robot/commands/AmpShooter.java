// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.IntakeShooterSubsystem;
import frc.robot.Constants.IntakeShooterSubsystemConstants;


public class AmpShooter extends Command {
  private IntakeShooterSubsystem m_Shooter;
  private int m_finishCount;

  /** Creates a new AmpShooter. */
  public AmpShooter(IntakeShooterSubsystem p_IntakeShooter) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(p_IntakeShooter);
    m_Shooter = p_IntakeShooter;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
   m_finishCount = 0;
   m_Shooter.setShooterWheelsSpeed(IntakeShooterSubsystemConstants.kLeftShooterAmpPercentage, IntakeShooterSubsystemConstants.kRightShooterAmpPercentage);
   m_Shooter.setShooterToAmp();
   
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if(m_Shooter.checkShooterInPosition(IntakeShooterSubsystemConstants.kShooterRotationsAmp)){
      m_Shooter.setHopupPercentage(IntakeShooterSubsystemConstants.kHopUpAmpPercentage);
      m_Shooter.setCatcherPercentage(IntakeShooterSubsystemConstants.kCatcherAmpPercentage);
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    m_Shooter.setShooterWheelsSpeed(IntakeShooterSubsystemConstants.kMotorOffSpeed, IntakeShooterSubsystemConstants.kMotorOffSpeed);
    m_Shooter.setHopupPercentage(IntakeShooterSubsystemConstants.kMotorOffSpeed);
    m_Shooter.setCatcherPercentage(IntakeShooterSubsystemConstants.kMotorOffSpeed);
    m_Shooter.setShooterToStowed();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    //Check if both note sensors are unblocked
    if (!(m_Shooter.getIntakeFromGroundSensorBlocked()) && !(m_Shooter.getIntakeFromSourceSensorBlocked()))
      m_finishCount++;

    if(m_finishCount == 25)
      return true;
    else 
      return false;
  }
}


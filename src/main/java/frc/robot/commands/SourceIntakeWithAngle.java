// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import java.util.function.DoubleSupplier;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Constants.Drivebase;
import frc.robot.Constants.IntakeShooterSubsystemConstants;
import frc.robot.subsystems.BlinkSubSystem;
import frc.robot.subsystems.IntakeShooterSubsystem;
import frc.robot.subsystems.swervedrive.SwerveSubsystem;
import edu.wpi.first.wpilibj.Servo;

public class SourceIntakeWithAngle extends Command {
  private IntakeShooterSubsystem m_shooter;
  private SwerveSubsystem m_swerve;
  private DoubleSupplier m_translationX;
  private DoubleSupplier m_translationY;
  private PIDController m_rotationController;
  private boolean m_isRedAlliance;
  private double m_rotationAngle;
  private double m_rotationError;
  //private BlinkSubSystem m_Servo;

  /** Creates a new SourceIntakeWithAngle. */
  public SourceIntakeWithAngle(IntakeShooterSubsystem p_shooter, SwerveSubsystem p_swerve, boolean p_isRedAlliance,
      DoubleSupplier p_translationX, DoubleSupplier p_translationY) {
    //m_Servo = p_Servo;
    m_shooter = p_shooter;
    m_swerve = p_swerve;
    m_isRedAlliance = p_isRedAlliance;
    m_translationX = p_translationX;
    m_translationY = p_translationY;
    m_rotationController = Drivebase.ROTATION_PID_CONTROLLER;

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(p_swerve, p_shooter);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {

    // turn on motors
    m_shooter.setShooterToSourcePickup();
    m_shooter.setShooterWheelsSpeed(IntakeShooterSubsystemConstants.kShooterPickupFromSourceSpeed,
        IntakeShooterSubsystemConstants.kShooterPickupFromSourceSpeed);
    m_shooter.setHopupPercentage(IntakeShooterSubsystemConstants.kHopupPickupFromSourceSpeed);
    m_shooter.setCatcherPercentage(IntakeShooterSubsystemConstants.kCatcherPickupFromSourceSpeed);

    // Set up rotational PID
    m_rotationController.enableContinuousInput(-Math.PI, Math.PI);
    m_rotationController.reset();

   
    // Set rotation angle based on alliance
    if (m_isRedAlliance)
      m_rotationAngle = Drivebase.kRedSourceRot;
    else
      m_rotationAngle = Drivebase.kBlueSourceRot;
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    m_rotationController.setSetpoint(Math.toRadians(m_rotationAngle));

    // Get current rotation
    m_rotationError = m_swerve.getPose().getRotation().getRadians();

    // Drive with set Source angle
    m_swerve.drive(new Translation2d((Math.pow(m_translationY.getAsDouble(), 3) * m_swerve.getMaxVelocity()),
        Math.pow(m_translationX.getAsDouble(), 3) * m_swerve.getMaxVelocity()),
        m_rotationController.calculate(m_rotationError), true);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {

    while(!m_shooter.getIntakeFromGroundSensorBlocked()){
      m_shooter.setCatcherPercentage(IntakeShooterSubsystemConstants.ksourceReverseSpeed);
      m_shooter.setHopupPercentage(IntakeShooterSubsystemConstants.ksourceReverseSpeed);
    }
    while(!m_shooter.getIntakeFromSourceSensorBlocked()){
      m_shooter.setCatcherPercentage(IntakeShooterSubsystemConstants.kgroundHopUpReverseSpeed);
      m_shooter.setHopupPercentage(IntakeShooterSubsystemConstants.kgroundHopUpReverseSpeed);
    }
   m_shooter.setShooterWheelsSpeed(IntakeShooterSubsystemConstants.kMotorOffSpeed, IntakeShooterSubsystemConstants.kMotorOffSpeed);
   m_shooter.setHopupPercentage(IntakeShooterSubsystemConstants.kMotorOffSpeed);
   m_shooter.setCatcherPercentage(IntakeShooterSubsystemConstants.kMotorOffSpeed);
   m_shooter.setShooterToStowed();

  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return m_shooter.getIntakeFromSourceSensorBlocked();
    
  }
}

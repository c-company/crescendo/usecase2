// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import java.util.function.DoubleSupplier;

import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.swervedrive.SwerveSubsystem;

public class Quarterspeed extends Command {
  SwerveSubsystem m_swerve;
  DoubleSupplier m_translationX;
  DoubleSupplier m_translationY;
  DoubleSupplier m_rotation;

  /** Creates a new Quarterspeed. */
  public Quarterspeed(SwerveSubsystem p_swerve, DoubleSupplier p_translationY, DoubleSupplier p_translationX, DoubleSupplier p_rotation) {
    m_swerve = p_swerve;
    m_translationX = p_translationX;
    m_translationY = p_translationY;
    m_rotation = p_rotation;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(p_swerve);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {

  m_swerve.drive(new Translation2d((Math.pow(m_translationY.getAsDouble(), 3) * (m_swerve.getMaxVelocity() / 6)),
      Math.pow(m_translationX.getAsDouble(), 3) * (m_swerve.getMaxVelocity() / 6)),
      Math.pow(m_rotation.getAsDouble(), 3) * (m_swerve.getMaxAngularVelocity() / 6), true);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}

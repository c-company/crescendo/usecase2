// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Constants.IntakeShooterSubsystemConstants;
import frc.robot.subsystems.IntakeShooterSubsystem;


public class DumbShooter extends Command {
  IntakeShooterSubsystem m_Shooter;
  boolean m_finished;
  int m_counter;
  int m_finishCount;

  /** Creates a new DumbShooter. */
  public DumbShooter(IntakeShooterSubsystem p_shooter) {
    m_Shooter = p_shooter;

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(p_shooter);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    m_Shooter.setShooterToManualSpeaker();
    m_Shooter.setShooterWheelsSpeed(IntakeShooterSubsystemConstants.kShootMotorFullSpeed, 
      IntakeShooterSubsystemConstants.kShootMotorFullSpeed);
    m_counter = 0;
    m_finishCount = 0;
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if (m_Shooter.checkShooterFullSpeed() || (m_counter == 50)){
      m_Shooter.setHopupPercentage(IntakeShooterSubsystemConstants.kShootMotorFullSpeed);
      m_Shooter.setCatcherPercentage(IntakeShooterSubsystemConstants.kShootMotorFullSpeed);
    }
    m_counter++;
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
   
    m_Shooter.setShooterWheelsSpeed(IntakeShooterSubsystemConstants.kMotorOffSpeed, IntakeShooterSubsystemConstants.kMotorOffSpeed);
    m_Shooter.setHopupPercentage(IntakeShooterSubsystemConstants.kMotorOffSpeed);
    m_Shooter.setCatcherPercentage(IntakeShooterSubsystemConstants.kMotorOffSpeed);
    m_Shooter.setShooterToStowed();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    if (!(m_Shooter.getIntakeFromGroundSensorBlocked()) && !(m_Shooter.getIntakeFromSourceSensorBlocked()))
      m_finishCount++;

  if(m_finishCount == 10)
    return true;
  else 
    return false;
  }
}

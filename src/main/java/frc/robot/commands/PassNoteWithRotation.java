// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import java.util.function.DoubleSupplier;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Constants.Drivebase;
import frc.robot.Constants.IntakeShooterSubsystemConstants;
import frc.robot.subsystems.IntakeShooterSubsystem;
import frc.robot.subsystems.swervedrive.SwerveSubsystem;

public class PassNoteWithRotation extends Command {
  private SwerveSubsystem m_swerve;
  private IntakeShooterSubsystem m_shooter;
  private DoubleSupplier m_translationX;
  private DoubleSupplier m_translationY;
  private PIDController m_rotationController;
  private double m_rotationError;
  private int m_finishCount;
  private int m_fullSpeedCount;
  private boolean m_isRedAlliance;
  private double m_rotationAngle;

  /** Creates a new PassNote. */
  public PassNoteWithRotation(SwerveSubsystem p_swerve, IntakeShooterSubsystem p_shooter, boolean p_isRedAlliance,
      DoubleSupplier p_translationX, DoubleSupplier p_translationY) {
    m_swerve = p_swerve;
    m_shooter = p_shooter;
    m_isRedAlliance = p_isRedAlliance;
    m_translationX = p_translationX;
    m_translationY = p_translationY;
    m_rotationController = Drivebase.ROTATION_PID_CONTROLLER;

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(p_swerve, p_shooter);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {

    // turn shooter wheels on
    m_shooter.setShooterWheelsSpeed(IntakeShooterSubsystemConstants.kShootMotorFullSpeed,
        IntakeShooterSubsystemConstants.kShootMotorFullSpeed);

    m_shooter.setShooterSetPoint(true, IntakeShooterSubsystemConstants.kShooterAnglePassNote);

    // Set up rotational PID
    m_rotationController.enableContinuousInput(-Math.PI, Math.PI);
    m_rotationController.reset();

    // initialize counters
    m_finishCount = 0;
    m_fullSpeedCount = 0;

  } // end initialize

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {

    // Find angle to speaker
    if (m_isRedAlliance) { // if Red
      m_rotationAngle = 180 + Math.atan((Drivebase.kRedPassY - m_swerve.getPose().getY()) /
          (Drivebase.kRedPassX - m_swerve.getPose().getX()));
    } else { // if Blue
      m_rotationAngle = -Math.atan((Drivebase.kBluePassY - m_swerve.getPose().getY()) /
          (m_swerve.getPose().getX() - Drivebase.kBluePassX));
    }

    // Set rotation setpoint to point towards Speaker
    m_rotationController.setSetpoint(Math.toRadians(m_rotationAngle));

    // Get current rotation
    m_rotationError = m_swerve.getPose().getRotation().getRadians();

    // Drive with rotation set to PassNote position
    m_swerve.drive(new Translation2d((Math.pow(m_translationY.getAsDouble(), 3) * m_swerve.getMaxVelocity()),
        Math.pow(m_translationX.getAsDouble(), 3) * m_swerve.getMaxVelocity()),
        m_rotationController.calculate(m_rotationError), true);

    // if rotation is within tolerance
    if ((Math.abs(m_swerve.getPose().getRotation().getDegrees()) - m_rotationAngle) <= 3) {
      // if Shooter angle is within tolerance
      if ((Math.abs(m_shooter.GetShooterAngleDegree())
          - IntakeShooterSubsystemConstants.kShooterAnglePassNote) <= IntakeShooterSubsystemConstants.kShooterAngleToleranceDegrees) { // if1
        // if shooter is running full speed
        if (m_shooter.checkShooterFullSpeed() || (m_fullSpeedCount >= 50)) { // if3
          m_shooter.setHopupPercentage(IntakeShooterSubsystemConstants.kHopupSpeakerSpeed);
          m_shooter.setCatcherPercentage(IntakeShooterSubsystemConstants.kShootMotorFullSpeed);
        } // end if3
        m_fullSpeedCount++;
      } // end if2
    } // end if1

  } // end execute

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {

    // turn off motors
    m_shooter.setShooterWheelsSpeed(IntakeShooterSubsystemConstants.kMotorOffSpeed,
        IntakeShooterSubsystemConstants.kMotorOffSpeed);
    m_shooter.disableHopupMotor();
    m_shooter.setCatcherPercentage(IntakeShooterSubsystemConstants.kMotorOffSpeed);

    // stow shooter
    m_shooter.setShooterToStowed();

  } // end end

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {

    // wait for 0.5 seconds after both sensors are clear before ending
    if (!(m_shooter.getIntakeFromGroundSensorBlocked()) && !(m_shooter.getIntakeFromSourceSensorBlocked()))
      m_finishCount++;

    if (m_finishCount == 3)
      return true;
    else
      return false;

  } // end isFinished

} // End TrackAndShoot

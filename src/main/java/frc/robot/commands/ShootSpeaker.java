// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.IntakeShooterSubsystem;
import frc.robot.Constants.IntakeShooterSubsystemConstants;
import frc.robot.subsystems.LimelightHelpers;
import frc.robot.Constants.LimeLightConstants;

public class ShootSpeaker extends Command {
  private IntakeShooterSubsystem m_IntakeShooterSubsystem;
  private int m_finishCount;
  private int m_counter;
  private double m_distance;
  private double m_offset;
  private double m_speakerSetpoint;
  private int m_LLwait;
  private int m_shooterSettleCount;

  /** Creates a new ShootSpeaker. */
  public ShootSpeaker(IntakeShooterSubsystem p_IntakeShooterSubsystem) {

    m_IntakeShooterSubsystem = p_IntakeShooterSubsystem;

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(p_IntakeShooterSubsystem);

  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {

    // set LL Pipeline to Speaker
    LimelightHelpers.setPipelineIndex(LimeLightConstants.kLimelightName, LimeLightConstants.kSpeakerPipeline);

    m_finishCount = 0;
    m_counter = 0;
    m_offset = 0;
    m_LLwait = 0;
    m_shooterSettleCount = 0;

    // turn shooter wheels on
    m_IntakeShooterSubsystem.setShooterWheelsSpeed(IntakeShooterSubsystemConstants.kShootMotorFullSpeed,
        IntakeShooterSubsystemConstants.kShootMotorFullSpeed);

  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {

    if (m_LLwait > 10) {
      m_distance = m_IntakeShooterSubsystem.GetSpeakerDistance();

    // Get custom angle offset
    m_offset = m_IntakeShooterSubsystem.calculateAngleOffset(m_distance);
    //m_offset = 3.5;


    // calculate shooter setpoint with offset
    m_speakerSetpoint = 90 - IntakeShooterSubsystemConstants.kShooterAngleOffset - m_offset
        - m_IntakeShooterSubsystem.getSpeakerAngle();

    // set shooter angle setpoint
    m_IntakeShooterSubsystem.setShooterSetPoint(true, m_speakerSetpoint);

    if (m_IntakeShooterSubsystem.checkShooterFullSpeed() || (m_counter >= 25)) {
      if ((Math.abs(m_IntakeShooterSubsystem.GetShooterAngleDegree())
          - m_speakerSetpoint) <= IntakeShooterSubsystemConstants.kShooterAngleToleranceDegrees) {
        if (m_shooterSettleCount == 5) {
        System.out.println("Distance: " + String.valueOf(m_distance));
        System.out.println("Offset: " + String.valueOf(m_offset));
        System.out.println("ShooterAngle: " + String.valueOf(m_speakerSetpoint));
        m_IntakeShooterSubsystem.setHopupPercentage(IntakeShooterSubsystemConstants.kHopupSpeakerSpeed);
        m_IntakeShooterSubsystem.setCatcherPercentage(IntakeShooterSubsystemConstants.kShootMotorFullSpeed);
        }
        m_shooterSettleCount++;
      }
    }

    m_counter++;
    System.out.println("execute counter: " + String.valueOf(m_counter));
  }

  m_LLwait++;

  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {

    // turn off motors
    m_IntakeShooterSubsystem.setShooterWheelsSpeed(IntakeShooterSubsystemConstants.kMotorOffSpeed,
        IntakeShooterSubsystemConstants.kMotorOffSpeed);
    m_IntakeShooterSubsystem.disableHopupMotor();
    m_IntakeShooterSubsystem.setCatcherPercentage(IntakeShooterSubsystemConstants.kMotorOffSpeed);

    // stow shooter
    m_IntakeShooterSubsystem.setShooterToStowed();

    // set LL back to all tags pipeline
    LimelightHelpers.setPipelineIndex(LimeLightConstants.kLimelightName, LimeLightConstants.kAllAprilTagPipeline);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    if (!(m_IntakeShooterSubsystem.getIntakeFromGroundSensorBlocked())
        && !(m_IntakeShooterSubsystem.getIntakeFromSourceSensorBlocked()))
      m_finishCount++;

    if (m_finishCount == 25)
      return true;
    else
      return false;
  }
}